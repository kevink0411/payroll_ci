<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Valdo | Payrol </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="<?php echo base_url() ?>assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <?php echo $html['css']; ?>


    </head>
    <body class="transition skin-green" >

        <div class="wrapper">
            <?php echo $template['header']; ?>
            <aside class="main-sidebar ">
                <section class="sidebar">
                    <?php echo $template['sidebarmenu']; ?>
                </section>
            </aside>
            <div id="rightcolumn" class="content-wrapper">
                <section  class="content-header">
                    <h1>Pesan
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Payrol</li>
                    </ol>
                </section>
                <div class="box"><br>
                    <div class="col-md-4 col-xs-4"> 


                        <form action="<?php echo base_url() ?>upload" method="POST" onsubmit="ShowLoading()"  enctype="multipart/form-data">
                            <table border="1">
                                <tr>
                                    <td><input type="file" height="100" required class="custom-file-input" name="file"/></td>
                                    <td><input class="btn btn-dropbox" type="submit" value="upload" name="submit" /></td>
                                </tr>
                            </table>
                        </form> 
                    </div> 
                    <div class="col-md-2 col-xs-2"> 
                        
                    </div>    <br><br>  
                    <div class="clearfix"></div>
                    <form action="<?= base_url() ?>/payrol/export_payrol/export_data_bank" method="GET">
                        <?php
                        $ju = count($bank);
                        if ($ju > 0) {
                            ?>
                            <div class="col-md-1 col-xs-1"> 
                                <label>Bank Report</label>
                            </div>
                        <input hidden="" name="token" value="<?php echo sha1(date("mYd")); ?>">
                            <div class="col-md-2 col-xs-2"> 
                                <select required="" class="form-control" name="bank">
                                    <option value="">--PILIH BANK--</option>
                                    <?php for ($x = 0; $x < $ju; $x++) { ?>
                                        <option value="<?php echo $bank[$x]['bank'] ?>"><?php echo $bank[$x]['bank'] ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <input type="text" class="form-control" placeholder="dd/mm/YYYY" name="tanggal" id="datepicker"/>

                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <select required="" class="form-control" name="form">
                                    <option value="">--PRINT SEBAGAI--</option>
                                    <option value="surat1">Surat VSDM</option>
                                    <option value="surat2">Surat VSI</option>
                                    <option value="surat3">Surat VI</option>
                                    <option value="bank">Data Bank Excel</option>
                                </select> 

                            </div>
                            <!-- <div class="col-md-2 col-xs-2"> 
                                <input type="radio" name="hrd" value="VI"> VI
                                <input type="radio" name="hrd" value="VSI"> VSI
                                <input type="radio" name="hrd" value="VSDM"> VSDM 

                            </div>-->
                            <div class="col-md-2 col-xs-2"> 
                                <input class="btn btn-primary" type="submit" value="Proses"  name="submit" />
                            </div> <?php } ?>
                    </form>
                    <div class="clearfix"></div>
                    <br>
                    <form action="<?= base_url() ?>/payrol/export_payrol/export_data_perbulan" method="GET">
                        <?php
                        $juma = count($bulan);
                        $ja = count($client);
                        if ($juma > 0) {
                            ?>
                            <div class="col-md-1 col-xs-1"> 
                                <label>Monthly Report</label>
                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <select id="sc_get_prov" required="" class="form-control" name="bulan"> 
                                    <?php
                                    echo '<option value="" >--PILIH BULAN--</option>';
                                    foreach ($dropdownprov as $a) {
                                        echo '<option value="' . $a["bulan"] . '">' . $a["bulan"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <select id="sc_show_kota" class="form-control" name="klien"> 
                                    <?php
                                    echo '<option value="">--PILIH KLIEN--</option>';
                                    ?>
                                </select> 
                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <select class="form-control" required="" name="form">
                                    <option value="">--PRINT SEBAGAI--</option>
                                    <option value="payrol">Data Payrol</option>
                                    <option value="slip">Slip Gaji</option>
                                    <option value="summary">Sumary</option>
                                    <option value="pajak">Data Report Pajak</option>
                                    <option value="databankpay">Data Bank Payment</option>
                                </select> 
                            </div>
                            <div class="col-md-2 col-xs-2"> 
                                <input class="btn btn-primary" type="submit" value="Proses" name="submit" />
                            </div> <?php } ?>
                        <div class="clearfix"></div>
                        <div id="data" class="box-body"><table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>NIK</th>
										 <th>Payment Date</th>
                                        <th style="width:130px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div><div class="clearfix"></div>
                        <a  href="<?= base_url() ?>/assets/template_upload.xlsx"> <button type="button" class="btn btn-su" style="margin-left: 5px;">
                                <i class="fa fa-download"></i> Download Template
                            </button></a>
                    </form>

                    <script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js') ?>"></script>
                    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap-datepicker.min.js') ?>"></script>

                    <div class="clearfix"></div>
                    <div class="box-body">
                        <script type="text/javascript">
                            var save_method;
                            var table;

                            $(document).ready(function () {
                                table = $('#table').DataTable({
                                    "processing": false,
                                    "serverSide": true,
                                    "order": [],
                                    "ajax": {
                                        "url": "<?php echo site_url('payrol/list_payrol/ajax_list') ?>",
                                        "type": "POST"
                                    },
                                    "columnDefs": [
                                        {
                                            "targets": [-1],
                                            "orderable": true,
                                        },
                                    ],
                                });
                                $('.datepicker').datepicker({
                                    autoclose: true,
                                    format: "dd-mm-yyyy",
                                    todayHighlight: true,
                                    orientation: "top auto",
                                    todayBtn: true,
                                    todayHighlight: true,
                                });

                                $("input").change(function () {
                                    $(this).parent().parent().removeClass('has-error');
                                    $(this).next().empty();
                                });

                                $("textarea").change(function () {
                                    $(this).parent().parent().removeClass('has-error');
                                    $(this).next().empty();
                                });

                                $("select").change(function () {
                                    $(this).parent().parent().removeClass('has-error');
                                    $(this).next().empty();
                                });
                            });



                            function edit_payrol(id_payrol)
                            {
                                save_method = 'update';
                                $('#formpayrol')[0].reset();
                                $('.form-group').removeClass('has-error');
                                $('.help-block').empty();
                                $.ajax({
                                    url: "<?php echo site_url('payrol/list_payrol/ajax_edit/') ?>" + id_payrol,
                                    type: "GET",
                                    dataType: "JSON",
                                    success: function (data)
                                    {
                                        $('[name="id_payrol"]').val(data.id_payrol);
                                        $('[name="nik"]').val(data.nik);
                                        $('[name="gaji_pokok"]').val(data.gaji_pokok);
                                        $('[name="rapel"]').val(data.rapel);
                                        $('[name="transport_tetap"]').val(data.transport_tetap);
                                        $('[name="transport_variable"]').val(data.transport_variable);
                                        $('[name="makan_variable"]').val(data.makan_variable);
                                        $('[name="lembur1"]').val(data.lembur1);
                                        $('[name="lembur2"]').val(data.lembur2);
                                        $('[name="total_lembur"]').val(data.total_lembur);
                                        $('[name="shift1"]').val(data.shift1);
                                        $('[name="shift2"]').val(data.shift2);
                                        $('[name="total_shift"]').val(data.total_shift);
                                        $('[name="insentif1"]').val(data.insentif1);
                                        $('[name="insentif2"]').val(data.insentif2);
                                        $('[name="insentif3"]').val(data.insentif3);
                                        $('[name="total_insentif"]').val(data.total_insentif);
                                        $('[name="bonus"]').val(data.bonus);
                                        $('[name="thr"]').val(data.thr);
                                        $('[name="lain_lain1"]').val(data.lain_lain1);
                                        $('[name="lain_lain2"]').val(data.lain_lain2);
                                        $('[name="lain_lain3"]').val(data.lain_lain3);
                                        $('[name="total_lain_lain"]').val(data.total_lain_lain);
                                        $('[name="gross"]').val(data.gross);
                                        $('[name="jamsostek_karyawan"]').val(data.jamsostek_karyawan);
                                        $('[name="jamsostek_perusahaan"]').val(data.jamsostek_perusahaan);
                                        $('[name="pensiun_karyawan"]').val(data.pensiun_karyawan);
                                        $('[name="pensiun_perusahaan"]').val(data.pensiun_perusahaan);
                                        $('[name="bpjs_karyawan"]').val(data.bpjs_karyawan);
                                        $('[name="bpjs_perusahaan"]').val(data.bpjs_perusahaan);
                                        $('[name="pph21"]').val(data.pph21);
                                        $('[name="thp"]').val(data.thp);
                                        $('[name="pot_pinjam"]').val(data.pot_pinjam);
                                        $('[name="pot_lain"]').val(data.pot_lain);
                                        $('[name="total_pot"]').val(data.total_pot);
                                        

                                        $('#modal_form_payrol').modal('show');
                                        $('.modal-title').text('Edit Payrol');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        alert('Error get data from ajax');
                                    }
                                });
                            }

                            function add_p()
                            {
                                save_method = 'add';
                                $('#formpayrol')[0].reset(); 
                                $('.form-group').removeClass('has-error'); 
                                $('.help-block').empty(); 
                                $('#modal_form_payrol').modal('show'); 
                                $('.modal-title').text('Add Person'); 
                            }


                            function save()
                            {
                                $('#btnSave').text('saving...');
                                $('#btnSave').attr('disabled', true);
                                var url;
                                if (save_method == 'add') {
                                    url = "<?php echo site_url('payrol/list_payrol/ajax_add') ?>";
                                } else {
                                    url = "<?php echo site_url('payrol/list_payrol/ajax_update') ?>";
                                }
                                $.ajax({
                                    url: url,
                                    type: "POST",
                                    data: $('#formpayrol').serialize(),
                                    dataType: "JSON",
                                    success: function (data)
                                    {
                                        if (data.status)
                                        {
                                            $('#modal_form_payrol').modal('hide');
                                           // $("#example1").load(" #example1");
                                            table.ajax.reload(null, false);
                                            //javascript:ajaxpage('<?php echo base_url() ?>hari-raya', 'rightcolumn');
                                        } else
                                        {
                                            for (var i = 0; i < data.inputerror.length; i++)
                                            {
                                                $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                                                $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                                            }
                                        }
                                        $('#btnSave').text('save');
                                        $('#btnSave').attr('disabled', false);
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        alert('Error adding / update data');
                                        $('#btnSave').text('save');
                                        $('#btnSave').attr('disabled', false);
                                    }
                                });
                            }
                            function delete_payrol(id_payrol)
                            {
                                if (confirm('Are you sure delete this data?'))
                                {
                                    // ajax delete data to database
                                    $.ajax({
                                        url: "<?php echo site_url('payrol/list_payrol/ajax_delete') ?>/" + id_payrol,
                                        type: "POST",
                                        dataType: "JSON",
                                        success: function (data)
                                        {
                                            $('#modal_form').modal('hide');
                                           //table.ajax.reload(null, false);
                                            $("#data").load(" #data");
                                        },
                                        error: function (jqXHR, textStatus, errorThrown)
                                        {
                                            alert('Error deleting data');
                                        }
                                    });
                                }
                            }
                        </script>
                        <div class="modal fade" id="modal_form_payrol" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h3 class="modal-title">Person Form</h3>
                                    </div>
                                    <div  class="modal-body formpayrol">
                                        <form action="#" id="formpayrol" class="form-horizontal">
                                            <input type="hidden" value="" name="id_payrol"/> 
                                            <div class="form-body">
                                                
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Nik</label>
                                                    <div class="col-md-9">
                                                        <input name="nik" disabled placeholder="nik" class="form-control" type="text">
                                                    </div>
                                                </div>

                                               <div class="form-group">
                                                    <label class="control-label col-md-3">GAJI POKOK</label>
                                                    <div class="col-md-9">
                                                        <input name="gaji_pokok"  placeholder="GAJI POKOK" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">RAPEL</label>
                                                    <div class="col-md-9">
                                                        <input name="rapel"  placeholder="RAPEL" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TRANSPORT TETAP</label>
                                                    <div class="col-md-9">
                                                        <input name="transport_tetap"  placeholder="TRANSPORT TETAP" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TRANSPORT VARIABLE</label>
                                                    <div class="col-md-9">
                                                        <input name="transport_variable"  placeholder="TRANSPORT VARIABLE" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">MAKAN VARIABLE</label>
                                                    <div class="col-md-9">
                                                        <input name="makan_variable"  placeholder="MAKAN VARIABLE" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">LEMBUR1</label>
                                                    <div class="col-md-9">
                                                        <input name="lembur1"  placeholder="LEMBUR1" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">LEMBUR2</label>
                                                    <div class="col-md-9">
                                                        <input name="lembur2"  placeholder="LEMBUR2" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TOTAL LEMBUR</label>
                                                    <div class="col-md-9">
                                                        <input name="total_lembur"  placeholder="TOTAL LEMBUR" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">SHIFT1</label>
                                                    <div class="col-md-9">
                                                        <input name="shift1"  placeholder="SHIFT1" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">SHIFT2</label>
                                                    <div class="col-md-9">
                                                        <input name="shift2"  placeholder="SHIFT2" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TOTAL SHIFT</label>
                                                    <div class="col-md-9">
                                                        <input name="total_shift"  placeholder="TOTAL SHIFT" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">INSENTIF1</label>
                                                    <div class="col-md-9">
                                                        <input name="insentif1"  placeholder="INSENTIF1" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">INSENTIF2</label>
                                                    <div class="col-md-9">
                                                        <input name="insentif2"  placeholder="INSENTIF2" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">INSENTIF3</label>
                                                    <div class="col-md-9">
                                                        <input name="insentif3"  placeholder="INSENTIF3" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TOTAL INSENTIF</label>
                                                    <div class="col-md-9">
                                                        <input name="total_insentif"  placeholder="TOTAL INSENTIF" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">BONUS</label>
                                                    <div class="col-md-9">
                                                        <input name="bonus"  placeholder="BONUS" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">THR</label>
                                                    <div class="col-md-9">
                                                        <input name="thr"  placeholder="THR" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">LAIN-LAIN 1</label>
                                                    <div class="col-md-9">
                                                        <input name="lain_lain1"  placeholder="LAIN-LAIN 1" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">LAIN-LAIN 2</label>
                                                    <div class="col-md-9">
                                                        <input name="lain_lain2"  placeholder="LAIN-LAIN 2" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">LAIN-LAIN 3</label>
                                                    <div class="col-md-9">
                                                        <input name="lain_lain3"  placeholder="LAIN-LAIN 3" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TOTAL LAIN-LAIN</label>
                                                    <div class="col-md-9">
                                                        <input name="total_lain_lain"  placeholder="TOTAL LAIN-LAIN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">GROSS</label>
                                                    <div class="col-md-9">
                                                        <input name="gross"  placeholder="GROSS" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">JAMSOSTEK KARYAWAN</label>
                                                    <div class="col-md-9">
                                                        <input name="jamsostek_karyawan"  placeholder="JAMSOSTEK KARYAWAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">JAMSOSTEK PERUSAHAAN</label>
                                                    <div class="col-md-9">
                                                        <input name="jamsostek_perusahaan"  placeholder="JAMSOSTEK PERUSAHAAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">PENSIUN KARYAWAN</label>
                                                    <div class="col-md-9">
                                                        <input name="pensiun_karyawan"  placeholder="PENSIUN KARYAWAN" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">PENSIUN PERUSAHAAN</label>
                                                    <div class="col-md-9">
                                                        <input name="pensiun_perusahaan"  placeholder="PENSIUN PERUSAHAAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">BPJS KARYAWAN</label>
                                                    <div class="col-md-9">
                                                        <input name="bpjs_karyawan"  placeholder="BPJS KARYAWAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">BPJS PERUSAHAAN</label>
                                                    <div class="col-md-9">
                                                        <input name="bpjs_perusahaan"  placeholder="BPJS PERUSAHAAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">PPH21</label>
                                                    <div class="col-md-9">
                                                        <input name="pph21"  placeholder="PPH21" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">THP</label>
                                                    <div class="col-md-9">
                                                        <input name="thp"  placeholder="THP" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label class="control-label col-md-3">POTONGAN PINJAMAN</label>
                                                    <div class="col-md-9">
                                                        <input name="pot_pinjam"  placeholder="POTONGAN PERUSAHAAN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">POTONGAN LAIN</label>
                                                    <div class="col-md-9">
                                                        <input name="pot_lain"  placeholder="POTONGAN LAIN" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">TOTAL POTONGAN</label>
                                                    <div class="col-md-9">
                                                        <input name="total_pot"  placeholder="TOTAL POTONGAN" class="form-control" type="text">
                                                    </div>
                                                </div>

                                        </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>    
            </div>
            <div class="clearfix"></div>
            <div class="control-sidebar-bg"></div>
        <?php echo $template['footer'] ?>
        <?php echo $html['js']; ?>
        </div>
        <script>
            $(document).ready(function () {
                $('#sc_get_par').change(function () {

                    var form_data = {
                        par_cat_id: $('#sc_get_par').val()
                    };
                    $.ajax({
                        url: "<?php echo site_url('payrol/list_payrol/ajax_call'); ?>",
                        type: 'POST',
                        dataType: 'json',
                        data: form_data,
                        success: function (msg) {
                            var sc = '';
                            $.each(msg, function (key, val) {
                                sc += '<option value="' + val.klien + '">' + val.klien + '</option>';
                            });
                            $("#sc_show_sub option").remove();
                            $("#sc_show_sub").append(sc);
                        }
                    });
                });
                $('#sc_get_prov').change(function () {
                    var form_data = {
                        bulan: $('#sc_get_prov').val()
                    };
                    $.ajax({
                        url: "<?php echo site_url('payrol/list_payrol/ajax_call_prov'); ?>",
                        type: 'POST',
                        dataType: 'json',
                        data: form_data,
                        success: function (msg) {
                            var sc = '';
                            $.each(msg, function (key, val) {
                                sc += '<option value="' + val.klien + '">' + val.klien + '</option>';
                            });
                            sc += '<option style="color:blue;" value="">ALL KLIEN</option>';
                            $("#sc_show_kota option").remove();
                            $("#sc_show_kota").append(sc);
                        }
                    });
                });
            });
        </script>


        
        <script>
            $('#datepicker').datetimepicker({
                format: 'd/m/Y',
                formatDate: 'd/m/Y'
            });
        </script>
        
         <script type="text/javascript">

            function ShowLoading(e) {
                var div = document.createElement('div');
                var img = document.createElement('img');
                img.src = '<?php echo base_url() ?>assets/images/load.gif';
                div.innerHTML = "Please Wait...<br />";
                div.style.cssText = 'position: fixed;  top: 50%; left: 400px; z-index: 5000; width: 422px; text-align: center; font-weight:bold; font-size:20px;';
                div.appendChild(img);
                document.body.appendChild(div);
               
                window.onscroll = function () {
                    window.scrollTo(0, 900);
                };
                return true;
            }
        </script>



    </body>
</html>