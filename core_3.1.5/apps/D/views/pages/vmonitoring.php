<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Valdo | Payrol System </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="<?php echo base_url() ?>assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <?php echo $html['css']; ?>
        <script>
            setInterval(function ()
            {
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url() ?>home",
                    datatype: "html",
                    success: function (data)
                    {
                        $("#activity").load(" #activity");
                    }
                });
            }, 5000);
        </script>
    </head>
    <body class="transition skin-green" >
        <div class="wrapper">
            <?php echo $template['header']; ?>
            <aside class="main-sidebar ">
                <section class="sidebar">
                    <?php echo $template['sidebarmenu']; ?>
                </section>
            </aside>
            <div id="rightcolumn" class="content-wrapper">
                <section class="content">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>0</h3>

                                    <p>0</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>0<sup style="font-size: 20px">%</sup></h3>

                                    <p>0</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>0</h3>

                                    <p>0</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>0</h3>

                                    <p>0</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                    <a href="<?php echo base_url() ?>/monitoring/monitoring/delete_all"><input class="btn btn-primary" type="submit" value="Delete all" name="submit" /></a>
                     <a href="<?php echo base_url() ?>/monitoring/monitoring/delete_by_id"><input class="btn btn-primary" type="submit" value="Delete Your Data" name="submit" /></a>
                    <!-- /.row -->
                   <div class="box"> 
                        <div class="box-header">
                          <h3 class="box-title">REPORT</h3>
                        </div>
                       <div class="box-body">
                        <table  id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>KLIEN</th>
                                    <th>TOTAL KARYAWAN</th>
                                    <th>PIC</th>
                                </tr>
                            </thead>
                            <tbody><?php
                                $jum = count($list);
                                for ($i = 0; $i < $jum; $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $list[$i]['KLIEN'] ?></td>
                                        <td><?php echo $list[$i]['TOTAL_KARYAWAN'] ?></td>
                                        <td><?php echo $list[$i]['NAMA'] ?></td>
                                    </tr> <?php } ?>
                            </tbody></table>
                    </div> </div>
                </section>
            </div>
            <?php echo $template['footer'] ?>
        </div>
        <?php echo $html['js']; ?>
<script>
            $.widget.bridge('uibutton', $.ui.button);
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>