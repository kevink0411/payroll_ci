<html xmlns:o="urn:schemas-microsoft-com:office:office" 
      xmlns:x="urn:schemas-microsoft-com:office:excel" 
      xmlns="http://www.w3.org/TR/REC-html40">
    <head><style>
            .s1 {mso-number-format:"#,##0.00_ ;[Red]-#,##0.00";}
            .s2 {mso-number-format:"#,##0.00_ ;[Red]-#,##0.00";}
        </style></head>
    <?php
    header("Content-type: application/octet-stream");
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=EXPORT-PAYROL.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    ?>
    <div>
    </div>
    <?php
    $total = count($alldata);
    echo"
    <table border='1'>
        <tbody>
        <tr>
        <th colspan='29' style='background-color:#adad85;font-size: 22;'>DATA PAYROL</th>  
         </tr>
        <tr>
        <th>NIK</th>
        <th>NAMA</th>
        <th>GAJI_POKOK</th>
        <th>RAPEL</th>
        <th>TRANSPORT_TETAP</th>
        <th>TRANSPORT_VARIABLE</th>
        <th>MAKAN_VARIABLE</th> 
        <th>TOTAL_LEMBUR</th>
        <th>TOTAL SHIFT</th>
        <th>TOTAL_INSENTIF</th>
        <th>BONUS</th>
        <th>THR</th>
        <th>TOTAL_LAIN_LAIN</th>
        <th>GROSS</th>
        <th>JAMSOSTEK_KARYAWAN</th>
        <th>JAMSOSTEK_PERUSAHAAN</th>
        <th>PENSIUN_KARYAWAN</th>
        <th>PENSIUN_PERUSAHAAN</th>
        <th>BPJS_KARYAWAN</th>
        <th>BPJS_PERUSAHAAN</th>
        <th>PPH21</th>
        <th>THP</th>
        <th>KLIEN</th>
        <th>BULAN</th>
        <th>REKENING</th>
        <th>BANK</th>
		<th>PAYMENT_DATE</th>
        <th>KETERANGAN</th>
        <th>HRD</th>
      </tr>";

    for ($i = 0; $i < $total; $i++) {
        echo "<tr  align='center'>
                    <td>" . $alldata[$i]['nik'] . "</td>
                    <td>" . $alldata[$i]['nama'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['gaji_pokok'] . "'>" . $alldata[$i]['gaji_pokok'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['rapel'] . "'>" . $alldata[$i]['rapel'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['transport_tetap'] . "'>" . $alldata[$i]['transport_tetap'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['transport_variable'] . "'>" . $alldata[$i]['transport_variable'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['makan_variable'] . "'>" . $alldata[$i]['makan_variable'] . "</td>   
                    <td class='s1' x:num='" . $alldata[$i]['total_lembur'] . "'>" . $alldata[$i]['total_lembur'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['total_shift'] . "'>" . $alldata[$i]['total_shift'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['total_insentif'] . "'>" . $alldata[$i]['total_insentif'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['bonus'] . "'>" . $alldata[$i]['bonus'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['thr'] . "'>" . $alldata[$i]['thr'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['total_lain_lain'] . "'>" . $alldata[$i]['total_lain_lain'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['gross'] . "'>" . $alldata[$i]['gross'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['jamsostek_karyawan'] . "'>" . $alldata[$i]['jamsostek_karyawan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['jamsostek_perusahaan'] . "'>" . $alldata[$i]['jamsostek_perusahaan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['pensiun_karyawan'] . "'>" . $alldata[$i]['pensiun_karyawan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['pensiun_perusahaan'] . "'>" . $alldata[$i]['pensiun_perusahaan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['bpjs_karyawan'] . "'>" . $alldata[$i]['bpjs_karyawan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['bpjs_perusahaan'] . "'>" . $alldata[$i]['bpjs_perusahaan'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['pph21'] . "'>" . $alldata[$i]['pph21'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['thp'] . "'>" . $alldata[$i]['thp'] . "</td>
                    <td>" . $alldata[$i]['klien'] . "</td>
                    <td>" . $alldata[$i]['bulan'] . "</td>
                    <td>" . $alldata[$i]['no_rekening'] . "</td>
                    <td>" . $alldata[$i]['bank'] . "</td>
					<td>" . $alldata[$i]['payment_date'] . "</td>
                    <td>" . $alldata[$i]['keterangan'] . "</td>
					<td>" . $alldata[$i]['hrd'] . "</td>
            </tr>";
    }
    echo "<tr style='background-color:#adad85;' >
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GAPOK'] . "'>" . $alldata[0]['TOTAL_GAPOK'] . "</td>
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_RAPEL'] . "'>" . $alldata[0]['TOTAL_RAPEL'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_MAKAN_VARIABLE'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_MAKAN_VARIABLE'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LEMBUR'] . "'>" . $alldata[0]['TOTAL_LEMBUR'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_SHIFT'] . "'>" . $alldata[0]['TOTAL_SHIFT'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_INSENTIF'] . "'>" . $alldata[0]['TOTAL_INSENTIF'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_BONUS'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_BONUS'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THR'] . "'>" . $alldata[0]['TOTAL_THR'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LAIN_LAIN'] . "'>" . $alldata[0]['TOTAL_LAIN_LAIN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GROSS'] . "'>" . $alldata[0]['TOTAL_GROSS'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PPH21'] . "'>" . $alldata[0]['TOTAL_PPH21'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THP'] . "'>" . $alldata[0]['TOTAL_THP'] . "</td> 
          
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center'>-</td>
		    <td align='center'>-</td> 
           <td align='center'>-</td>
		   <td align='center'>-</td>
         </tr>";
    echo "</tbody></table>";
    