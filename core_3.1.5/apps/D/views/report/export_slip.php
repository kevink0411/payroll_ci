<html xmlns:o="urn:schemas-microsoft-com:office:office" 
      xmlns:x="urn:schemas-microsoft-com:office:excel" 
      xmlns="http://www.w3.org/TR/REC-html40">
    <head><style>
        .s1 {mso-number-format:"#,##0.00_ ;[Red]-#,##0.00";}
    </style></head>
<?php

header("Content-type: application/octet-stream");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=SLIP-GAJI.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<div>
</div>
<?php
$no = 1;
$total = count($alldata);
echo"
    <table border='1'>
        <tbody>
        <tr>
        <th colspan='51'  style='background-color:#adad85;font-size: 22;'>SLIP GAJI</th>  
        </tr>
        <tr>
        <th style='background-color:#cccecc;'>S1-NoU</th>
        <th style='background-color:#cccecc;'>S1-BlnGaji</th>
        <th style='background-color:#cccecc;'>S1-ThnGaji</th>
        <th style='background-color:#cccecc;'>S1-Nama</th>
        <th style='background-color:#cccecc;'>S1-NIK</th>
        <th style='background-color:#cccecc;'>S1-NPWP</th>
        <th style='background-color:#cccecc;'>S1-Dept</th> 
        <th style='background-color:#cccecc;'>S1-Jabatan</th>
        <th style='background-color:#cccecc;'>S1-Band</th>
        <th style='background-color:#cccecc;'>S1_NOBPJSTK</th>
        <th style='background-color:#cccecc;'>S1-Cabang</th>
        <th style='background-color:#ef2828;'>S1-TglMasuk</th>
        <th style='background-color:#cccecc;'>S1-StatKel</th>
        <th style='background-color:#cccecc;'>S1-NOBPJSKes</th>
        
        <th style='background-color:#efbd88;'> S2-UpahPokok</th>
        <th style='background-color:#efbd88;'> S2-TunjTransTetap </th>
        <th style='background-color:#efbd88;'> S2-TunjTransVar </th>
        <th style='background-color:#efbd88;'> S2-TunjMakanVar </th>
        <th style='background-color:#efbd88;'> S2-TunjOT</th>
        <th style='background-color:#efbd88;'> S2-TunjShift </th>
        <th style='background-color:#efbd88;'> S2-TunjIncKom </th>
        <th style='background-color:#efbd88;'> S2-TunjBonus </th>
       <th style='background-color:#ef2828;'>S2-TunjBPJS-TK </th>
        <th style='background-color:#ef2828;'> S2-TunjBPJS-Kes</th>
        <th style='background-color:#ef2828;'> S2-TunjBPJS-Pens </th>
        <th style='background-color:#efbd88;'> S2-TunjTHR </th>
        <th style='background-color:#efbd88;'> S2-TunjLain2 </th>
        <th style='background-color:#efbd88;'> S2-TunjRapel </th>
       <th style='background-color:#efbd88;'> S2-TotPenghKotor </th>
       <th style='background-color:#e8ef27;'> S3-PotKetdkHdrn </th>
        <th style='background-color:#e8ef27;'> S3-PotPBJS-TK </th>
        <th style='background-color:#e8ef27;'> S3-PotBPJS-Kes </th>
        <th style='background-color:#e8ef27;'> S3-PotBPJS-Pens </th>
        <th style='background-color:#e8ef27;'> S3-PotPph21 </th>
        <th style='background-color:#e8ef27;'> S3-PotPinjam </th>
        <th style='background-color:#e8ef27;'>S3-PotLain2 </th>
       <th style='background-color:#e8ef27;'> S3-TotPot </th>
       <th style='background-color:#e8ef27;'> S3-TotPenghTerima </th>
        <th style='background-color:#e8ef27;'>S3-HRD</th>
        <th></th>
        <th style='background-color:#9cb8e5;'>PAYROL</th>
        <th style='background-color:#9cb8e5;'>SELISIH</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th style='background-color:#ef2828;'>DOB</th>
        <th>Email</th>
        <th>Klien</th>
        <th>Department</th>
        <th>cabang</th>
        
      </tr>";
for ($i = 0; $i < $total; $i++) {
    echo "<tr align='center'>
                    <td>" . $no . "</td>
                    <td>" . $alldata[$i]['bulan'] . "</td>
                    <td>" . $alldata[$i]['tahun'] . "</td>
                    <td>" . $alldata[$i]['nama'] . "</td>
                    <td class='s2' x:num='". $alldata[$i]['nik']."'>" . $alldata[$i]['nik'] . "</td>
                    <td class='s2' x:num='". $alldata[$i]['no_npwp']."'>" . $alldata[$i]['no_npwp'] . "</td>
                    <td>" . $alldata[$i]['dept'] . "</td>
                    <td>" . $alldata[$i]['jabatan'] . "</td>
                    <td>" . $alldata[$i]['band'] . "</td>
                    <td class='s2' x:num='". $alldata[$i]['no_bpjs_tk']."'>" . $alldata[$i]['no_bpjs_tk'] . "</td>
                    <td>".$alldata[$i]['cabang']."</td> 
                    <td>" . $alldata[$i]['tgl_masuk'] . "</td>
                    <td>" . $alldata[$i]['status_pajak'] . "</td>
                    <td class='s2' x:num='". $alldata[$i]['no_bpjs_kes']."'>" . $alldata[$i]['no_bpjs_kes'] . "</td>
                    
                        
                    <td class='s1' x:num='". $alldata[$i]['gaji_pokok']."'>" .  $alldata[$i]['gaji_pokok']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['transport_tetap']."'>" .  $alldata[$i]['transport_tetap']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['transport_variable']."'>" .  $alldata[$i]['transport_variable']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['makan_variable']."'>" .  $alldata[$i]['makan_variable']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['total_lembur']."'>" .  $alldata[$i]['total_lembur']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['total_shift']."'>" .  $alldata[$i]['total_shift']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['total_insentif']."'>" .  $alldata[$i]['total_insentif']  . "</td>    
                    <td class='s1' x:num='". $alldata[$i]['bonus']."'>" .  $alldata[$i]['bonus']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['jamsostek_karyawan']."'>" .  $alldata[$i]['jamsostek_karyawan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['bpjs_karyawan']."'>" .  $alldata[$i]['bpjs_karyawan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['pensiun_karyawan']."'>" .  $alldata[$i]['pensiun_karyawan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['thr']."'>" .  $alldata[$i]['thr']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['total_lain_lain']."'>" .  $alldata[$i]['total_lain_lain']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['rapel']."'>" .  $alldata[$i]['rapel']  . "</td>   
                    <td class='s1' x:num='". $alldata[$i]['gross']."'>" .  $alldata[$i]['gross']  . "</td>
                    <td>0</td>
                    <td class='s1' x:num='". $alldata[$i]['jamsostek_perusahaan']."'>" .  $alldata[$i]['jamsostek_perusahaan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['bpjs_perusahaan']."'>" .  $alldata[$i]['bpjs_perusahaan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['pensiun_perusahaan']."'>" .  $alldata[$i]['pensiun_perusahaan']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['pph21']."'>" .  $alldata[$i]['pph21']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['pot_pinjam']."'>" .  $alldata[$i]['pot_pinjam']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['pot_lain']."'>" .  $alldata[$i]['pot_lain']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['total_pot']."'>" .  $alldata[$i]['total_pot']  . "</td>
                    <td class='s1' x:num='". $alldata[$i]['thp']."'>" .  $alldata[$i]['thp']  . "</td>
                    <td>".$alldata[$i]['hrd']."</td> 
                    <td></td> 
                    <td>0</td> 
                    <td>0</td> 
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <td>".$alldata[$i]['tgl_lahir']."</td> 
                    <td>".$alldata[$i]['alamat_email']."</td> 
                    <td>".$alldata[$i]['klien']."</td> 
                    <td>".$alldata[$i]['dept']."</td> 
                     <td>".$alldata[$i]['cabang']."</td> 
            </tr>";
    $no++;
}
 echo "<tr >
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GAPOK'] . "'>" . $alldata[0]['TOTAL_GAPOK'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_MAKAN_VARIABLE'] . "'>" . $alldata[0]['TOTAL_MAKAN_VARIABLE'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LEMBUR'] . "'>" . $alldata[0]['TOTAL_LEMBUR'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_SHIFT'] . "'>" . $alldata[0]['TOTAL_SHIFT'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_INSENTIF'] . "'>" . $alldata[0]['TOTAL_INSENTIF'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BONUS'] . "'>" . $alldata[0]['TOTAL_BONUS'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THR'] . "'>" . $alldata[0]['TOTAL_THR'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LAIN_LAIN'] . "'>" . $alldata[0]['TOTAL_LAIN_LAIN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_RAPEL'] . "'>" . $alldata[0]['TOTAL_RAPEL'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GROSS'] . "'>" . $alldata[0]['TOTAL_GROSS'] . "</td> 
           <td>-</td>
          <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PPH21'] . "'>" . $alldata[0]['TOTAL_PPH21'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_POT_PINJAM'] . "'>" . $alldata[0]['TOTAL_POT_PINJAM'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_POTONGAN_LAIN'] . "'>" . $alldata[0]['TOTAL_POTONGAN_LAIN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_POTONGAN'] . "'>" . $alldata[0]['TOTAL_POTONGAN'] . "</td> 
           <td style='background-color:#adad85;' align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THP'] . "'>" . $alldata[0]['TOTAL_THP'] . "</td> 

           <td align='center'>-</td> <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           
         </tr>";
        
echo "</tbody></table>";
