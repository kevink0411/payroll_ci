<header class="main-header">
    <a href="<?php echo base_url()?>" class="logo">
      <span class="logo-mini"><b>C</b>LI</span>
      <span class="logo-lg"><b>Valdo</b>Payrol</span>
</a>
    
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        
      </a>
         
      <div class="navbar-custom-menu">
          
        <ul class="nav navbar-nav">
           <li class="dropdown notifications-menu">
               <a href="javascript:void(0)" onclick="help()" >
              <i  class="fa fa-question"></i>
              <!--<span class="label label-warning">10</span>-->
            </a>
          </li>
          <li class="dropdown notifications-menu">
             <a href="<?php echo base_url() ?>logout" >
              <i  class="fa fa-user-times"></i>
              <!--<span class="label label-warning">10</span>-->
            </a>
          </li>
         
          
 
        </ul>
      </div>
    </nav>

</header> 
<div class="modal fade" id="modal_form_help" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            
            <div style="width:800px ;"  class="modal-body form">
                <p style="color:blue;">
                <b>Upload NB Baru</b><br>
                1. Jika sudah login, maka click menu <u>Upload</u><br>
                2. hanya bisa upload file dengan extention .xlsx<br>
                3. Berikut antarmuka untuk halaman upload<br>
                <img width="570" src="<?php echo base_url() ?>assets/images/help/upload.png"><br>
                - <b style="color:red;">All Polis Member</b> adalah jumlah keseluruhan nasabah atau pemegang polis yang sudah<br> diupload<br>
                - <b style="color:red;">New  Member</b> adalah jumlah keseluruhan nasabah atau pemegang polis yang belum <br> pernah di blast sms atau yang baru diupload<br>
                - <b style="color:red;">Success  Member</b> adalah jumlah keseluruhan nasabah atau pemegang polis yang sudah<br> berhasil di blast<br>
                - <b style="color:red;">Fail  Member</b> adalah jumlah keseluruhan nasabah atau pemegang polis yang gagal diblast<br> karena masalah jaringan dan lain2<br>
                - <b style="color:red;">Invalid Member</b> adalah jumlah keseluruhan nasabah atau pemegang polis yang nomor <br> yang salah,<br>
                - <b style="color:red;">No Number</b> adalah Member adalah jumlah keseluruhan nasabah atau pemegang polis yang<br> tidak mempunyai nomor hp<br>
                </p>
                <br><br>
                <b>NOTE: Jika Sudah berhasil di upload akan bertambah new member sesuai<br> jumlah data yang diupload</b>
                
                <form action="#" id="form_help" class="form-horizontal"></form>
                <BR>
                <b>Blast NB Baru</b><br>
                1. Jika sudah Upload data nasabah atau pemegang polis, maka click menu <b><u>NBUW</u>-><u>Polis</u></b><br>
                2. Berikut antarmuka untuk halaman upload<br>
                <img width="570" src="<?php echo base_url() ?>assets/images/help/blast.png"><br>
                <br>
                3. Button <button type="button" class="btn btn-primary" >Blast</button> digunakan untuk melakukan SMS Blast.
                saat ini default 20 sms per click.<br>
                4. Pastikan sampai <b>New</b> Dan <b>Fail</b> 0
                <br>
                5. Bug Report ke <a href="mailto:ferry.sukarto@valdo-intl.com?Subject=Hello%20Ferry%20Sukarto" target="_top">Ferry Sukarto</a>
            
<!--            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>-->
        </div>
    </div>
</div>
</div>
