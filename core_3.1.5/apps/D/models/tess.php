<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tess extends CI_Model {

    function get_log() {
        $sql = "SELECT * FROM td_admin_log";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
}