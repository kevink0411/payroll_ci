<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . '/third_party/PHPExcel/IOFactory.php';
require_once APPPATH . '/third_party/PHPExcel.php';

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('header', 'url', 'stringurl', 'security','textlimiter'));
        $this->load->library('form_validation');
        $this->load->model('mpesan','mpayrol');
    }

    public function index() {
        $a = array();
        $z = "";
        $b = "WHERE CHAR_LENGTH(status) > 3";
        $c = "WHERE status='1'";
        $d = "WHERE status='2'";
        $e = "WHERE status='5'";
        $f = "WHERE status='8'";
        $g = "WHERE status='7'";

        $a['total'] = $this->mpesan->get_all_member($z);
        $a['success'] = $this->mpesan->get_all_member($b);
        $a['invalid_number'] = $this->mpesan->get_all_member($c);
        $a['no_number'] = $this->mpesan->get_all_member($d);
        $a['ip_error'] = $this->mpesan->get_all_member($e);
        $a['new'] = $this->mpesan->get_all_member($f);
        $a['credit'] = $this->mpesan->get_all_member($g);

        
        $a['polis'] = $this->mpesan->td_get_polis();
        $a['member'] = $this->mpesan->td_get_member();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
         $a['html']['css'] .= add_css('/bootstrap/css/alertify.core.css');

        $a['html']['css'] .= add_css('/bootstrap/css/alertify.default.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');

        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');

        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/alertify.js');

        $a['html']['js'] .= add_js('/bootstrap/js/dataTables.bootstrap.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/plugins/sparkline/jquery.sparkline.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');

        $a['html']['js'] .= add_js('/plugins/chartjs/Chart.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');

        $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu', NULL, true);
        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, true);
         $a['pesan'] = '';
        if (isset($_POST["submit"])) {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    // echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($_FILES["file"]["name"])) {
                        unlink($_FILES["file"]["name"]);
                    }
                    $storagename = "assets/discussdesk.xlsx";
                    move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
                    $inputFileName = 'assets/discussdesk.xlsx';
                    try {
                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $arrayCount = count($allDataInSheet);
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        
                        $no =  addslashes(trim($allDataInSheet[$i]["A"]));
                        $nik = addslashes(trim($allDataInSheet[$i]["B"]));
                        $nama = addslashes(trim($allDataInSheet[$i]["C"]));
                        
                        $tgl_mas = addslashes(trim($allDataInSheet[$i]["D"]));
                        
                        $y=substr($tgl_mas, 0, 2);
                        $x=substr($tgl_mas, 3, 2);
                        $z=substr($tgl_mas, 6, 2);
                        $tgl_masuk = ($x.'/'.$y.'/20'.$z);
                        
                        
                        $tgl_keluar = addslashes(trim($allDataInSheet[$i]["E"]));
                        
                        $dept = addslashes(trim($allDataInSheet[$i]["F"]));
                        $jabatan = addslashes(trim($allDataInSheet[$i]["G"]));
                        $cabang = addslashes(trim($allDataInSheet[$i]["H"]));
                        $band = addslashes(trim($allDataInSheet[$i]["I"]));
                        $bank = addslashes(trim($allDataInSheet[$i]["J"]));
                        
                        $no_rekening = addslashes(trim($allDataInSheet[$i]["K"]));
                        $tlahir = addslashes(trim($allDataInSheet[$i]["L"]));
                        
                        $a=substr($tlahir, 3, 2);
                        $b=substr($tlahir, 0, 2);
                        $c=substr($tlahir, 6, 2);
                        $tgl_lahir = ($a.'/'.$b.'/19'.$c);
                        
                        $jns_kelamin = addslashes(trim($allDataInSheet[$i]["M"]));
                        $status_pegawai= addslashes(trim($allDataInSheet[$i]["N"]));
                        $status_pajak= addslashes(trim($allDataInSheet[$i]["O"]));
                        
                        $no_npwp= addslashes(trim($allDataInSheet[$i]["P"]));
                        $no_bpjs_kes= addslashes(trim($allDataInSheet[$i]["Q"]));
                        $no_bpjs_tk= addslashes(trim($allDataInSheet[$i]["R"]));
                        $no_ktp= addslashes(trim($allDataInSheet[$i]["S"]));
                        $alamat_rumah= addslashes(trim($allDataInSheet[$i]["T"]));
                        
                        $alamat_email= addslashes(trim($allDataInSheet[$i]["U"]));
                        $bulan = addslashes(trim($allDataInSheet[$i]["V"]));
                        $tahun = addslashes(trim($allDataInSheet[$i]["W"]));
                        $klien= addslashes(trim($allDataInSheet[$i]["X"]));
                        $gaji_pokok= addslashes(trim($allDataInSheet[$i]["Y"]));
                        
                        $rapel= addslashes(trim($allDataInSheet[$i]["Z"]));
                        $transport_tetap= addslashes(trim($allDataInSheet[$i]["AA"]));
                        $transport_variable= addslashes(trim($allDataInSheet[$i]["AB"]));
                        $makan_variable= addslashes(trim($allDataInSheet[$i]["AC"]));
                        $lembur1= addslashes(trim($allDataInSheet[$i]["AD"]));
                        
                        $lembur2= addslashes(trim($allDataInSheet[$i]["AE"]));
                        $total_lembur= addslashes(trim($allDataInSheet[$i]["AF"]));
                        $shift1= addslashes(trim($allDataInSheet[$i]["AG"]));
                        $shift2= addslashes(trim($allDataInSheet[$i]["AH"]));
                        $total_shift= addslashes(trim($allDataInSheet[$i]["AI"]));
                        
                        $insentif1= addslashes(trim($allDataInSheet[$i]["AJ"]));
                        $insentif2= addslashes(trim($allDataInSheet[$i]["AK"]));
                        $insentif3= addslashes(trim($allDataInSheet[$i]["AL"]));
                        $total_intensif= addslashes(trim($allDataInSheet[$i]["AM"]));
                        $bonus= addslashes(trim($allDataInSheet[$i]["AN"]));
                        
                        $thr= addslashes(trim($allDataInSheet[$i]["AO"]));
                        $lain_lain1= addslashes(trim($allDataInSheet[$i]["AP"]));
                        $lain_lain2= addslashes(trim($allDataInSheet[$i]["AQ"]));
                        $lain_lain3= addslashes(trim($allDataInSheet[$i]["AR"]));
                        $total_lain_lain= addslashes(trim($allDataInSheet[$i]["AS"]));
                        
                        $gross= addslashes(trim($allDataInSheet[$i]["AT"]));
                        $jamsostek_karyawan = addslashes(trim($allDataInSheet[$i]["AU"]));
                        $jamsostek_perusahaan = addslashes(trim($allDataInSheet[$i]["AV"]));
                        $pensiun_karyawan = addslashes(trim($allDataInSheet[$i]["AW"]));
                        $pensiun_perusahaan= addslashes(trim($allDataInSheet[$i]["AX"]));
                        
                        $bpjs_karyawan= addslashes(trim($allDataInSheet[$i]["AY"]));
                        $bpjs_perusahaan= addslashes(trim($allDataInSheet[$i]["AZ"]));
                        $pph21= addslashes(trim($allDataInSheet[$i]["BA"]));
                        $pot_pinjam= addslashes(trim($allDataInSheet[$i]["BB"]));
                        $pot_lain= addslashes(trim($allDataInSheet[$i]["BC"]));
                        
                        $total_pot= addslashes(trim($allDataInSheet[$i]["BD"]));
                        $thp= addslashes(trim($allDataInSheet[$i]["BE"]));
                        $hrd= addslashes(trim($allDataInSheet[$i]["BF"]));
                        $payment_dates= cleanWords(trim($allDataInSheet[$i]["BG"]));
                        $bul=substr($payment_dates, 0, 2);
                        $tgl=substr($payment_dates, 3, 2);
//                       / $th=substr($payment_dates, 6, 2);
                       
                      
                        $payment_date = ($bul.'/'.$tgl.'/2018');
                       
                       $keterangan= addslashes(trim($allDataInSheet[$i]["BH"]));
                       $n =$nik.$tgl."2018";
                       $query = $this->mpayrol->get_member_by_no($n);
                       
                      if ($query[0]['nik'] == 0) 
                            {  
                            $this->mpesan->upload(
                              $no,
                              $nik,$nama,
                              $tgl_masuk ,
                              $tgl_keluar ,
                              $dept ,

                              $jabatan ,
                              $cabang ,
                              $band ,
                              $bank ,
                              $no_rekening ,

                              $tgl_lahir ,
                              $jns_kelamin,
                              $status_pegawai,
                              $status_pajak,
                              $no_npwp,

                              $no_bpjs_kes,
                              $no_bpjs_tk,
                              $no_ktp,
                              $alamat_rumah,
                              $alamat_email,

                              $bulan,
                              $tahun ,
                              $klien,
                              $gaji_pokok,
                              $rapel,

                              $transport_tetap,
                              $transport_variable,
                              $makan_variable,
                              $lembur1,
                              $lembur2,

                              $total_lembur,
                              $shift1,
                              $shift2,
                              $total_shift,
                              $insentif1,

                              $insentif2,
                              $insentif3,
                              $total_intensif,
                              $bonus,
                              $thr,

                              $lain_lain1,
                              $lain_lain2,
                              $lain_lain3,
                              $total_lain_lain,
                              $gross,

                              $jamsostek_karyawan ,
                              $jamsostek_perusahaan ,
                              $pensiun_karyawan ,
                              $pensiun_perusahaan,
                              $bpjs_karyawan,

                              $bpjs_perusahaan,
                              $pph21,
                              $pot_pinjam,
                              $pot_lain,

                              $total_pot,
                              $thp,
                            $hrd,$payment_date,$keterangan);
//                             echo "<script>
//                            alert('Upload Success');
//                            window.location.href='".base_url()."payrol-data';
//                            </script>";
                            }
                            else{
                                
                                $this->mpesan->update_payrol(
                              $no,
                              $nik,$nama,
                              $tgl_masuk ,
                              $tgl_keluar ,
                              $dept ,

                              $jabatan ,
                              $cabang ,
                              $band ,
                              $bank ,
                              $no_rekening ,

                              $tgl_lahir ,
                              $jns_kelamin,
                              $status_pegawai,
                              $status_pajak,
                              $no_npwp,

                              $no_bpjs_kes,
                              $no_bpjs_tk,
                              $no_ktp,
                              $alamat_rumah,
                              $alamat_email,

                              $bulan,
                              $tahun ,
                              $klien,
                              $gaji_pokok,
                              $rapel,

                              $transport_tetap,
                              $transport_variable,
                              $makan_variable,
                              $lembur1,
                              $lembur2,

                              $total_lembur,
                              $shift1,
                              $shift2,
                              $total_shift,
                              $insentif1,

                              $insentif2,
                              $insentif3,
                              $total_intensif,
                              $bonus,
                              $thr,

                              $lain_lain1,
                              $lain_lain2,
                              $lain_lain3,
                              $total_lain_lain,
                              $gross,

                              $jamsostek_karyawan ,
                              $jamsostek_perusahaan ,
                              $pensiun_karyawan ,
                              $pensiun_perusahaan,
                              $bpjs_karyawan,

                              $bpjs_perusahaan,
                              $pph21,
                              $pot_pinjam,
                              $pot_lain,

                              $total_pot,
                              $thp,
                            $hrd,$payment_date,$keterangan,$n);
//                                  echo "<script>
//                            alert('Upload Fail');
//                            window.location.href='".base_url()."payrol-data';
//                            </script>";
                            }
                       
//                         $a['pesan'] ='<script>alert("You have Success Upload '.($arrayCount-1).' New Polis Data")</script>
//                          <strong><div class="alert alert-success btn-delay">
//                          <strong></strong> Success! Upload  '. ($arrayCount-1).' new member
//                        </div>';

                        
                    }
                
                }
            }
        } else {
          //  echo "No file selected <br />";
        }
   redirect('payrol-data','refresh');
        //$this->load->view('pages/vlist_payrol', $a, FALSE);
    }

    public function upload_transvis_template() {
        $a = array();
        $z = "";
        $b = "WHERE CHAR_LENGTH(status) > 3";
        $c = "WHERE status='1'";
        $d = "WHERE status='2'";
        $e = "WHERE status='5'";
        $f = "WHERE status='8'";
        $g = "WHERE status='7'";

        $a['total'] = $this->mpesan->get_all_member_payment($z);
        $a['success'] = $this->mpesan->get_all_member_payment($b);
        $a['invalid_number'] = $this->mpesan->get_all_member_payment($c);
        $a['no_number'] = $this->mpesan->get_all_member_payment($d);
        $a['ip_error'] = $this->mpesan->get_all_member_payment($e);
        $a['new'] = $this->mpesan->get_all_member_payment($f);
        $a['credit'] = $this->mpesan->get_all_member_payment($g);

        
        $a['polis'] = $this->mpesan->td_get_polis();
        $a['member'] = $this->mpesan->td_get_member();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
         $a['html']['css'] .= add_css('/bootstrap/css/alertify.core.css');

        $a['html']['css'] .= add_css('/bootstrap/css/alertify.default.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');

        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');

        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/alertify.js');

        $a['html']['js'] .= add_js('/bootstrap/js/dataTables.bootstrap.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/plugins/sparkline/jquery.sparkline.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');

        $a['html']['js'] .= add_js('/plugins/chartjs/Chart.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');

        //$id_group=$this->uri->segment(2);

        $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu', NULL, true);
        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, true);
         $a['pesan'] = '';
        if (isset($_POST["submit"])) {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    // echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($_FILES["file"]["name"])) {
                        unlink($_FILES["file"]["name"]);
                    }
                    $storagename = "assets/discussdesk.xlsx";
                    move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
                    $inputFileName = 'assets/discussdesk.xlsx';
                    try {
                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $arrayCount = count($allDataInSheet);
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        
                        $no_karyawan = trim($allDataInSheet[$i]["B"]);
                        $nama_karyawan = addslashes(trim($allDataInSheet[$i]["C"]));
                        $no_hp = trim($allDataInSheet[$i]["D"]);
                        $nominal_salah = trim($allDataInSheet[$i]["E"]);
                        $nominal_benar = trim($allDataInSheet[$i]["F"]);
                        $nominal_refund = trim($allDataInSheet[$i]["G"]);
//                        $query = $this->mpesan->get_member_by_no(str_replace("#", "", $no));
//                        if ($query[0]['no_polis'] == "") 
//                            {
                            $this->mpesan->upload_claim_pay($no_karyawan,$nama_karyawan,$no_hp,$nominal_salah,$nominal_benar,$nominal_refund);
                       
                         $a['pesan'] ='<script>alert("You have Success Upload '.($arrayCount-1).' New Polis Data")</script>
                          <strong><div class="alert alert-success btn-delay">
                          <strong></strong> Success! Upload  '. ($arrayCount-1).' new member
                        </div>';
                            
                        
                    }
                
                }
            }
        } else {
        }
        $this->load->view('pages/vupload_claim_payment', $a, FALSE);
    }
    
     function sms_mandiri()
        {
//            if('4297f44b13955235245b2497399d7a93'==$_GET['token'])
//            {
            $t['polis'] = $this->mpesan->td_get_polis();
            
            //var_dump( $t['polis'][0]['isi_pesan']);
            $t['member'] = $this->mpesan->td_get_member_polis();
            
//            var_dump($t['member']);
//            exit();
            if(count($t['member'])>0){
            for($i=0;$i<count($t['member']);$i++)
            {
                
                //$message = "Pemegang Polis Yth, Selamat bergabung Polis No ".$t['member'][$i]['no_polis']." telah disetujui. Hub CS 021-29023688 jika Anda belum menerima Polis dalam waktu 14 hari Terima kasih";
                $message =str_replace("[2]", $t['member'][$i]['no_polis'], $t['polis'][0]['isi_pesan']);
                $message_strip = str_replace(" ", "+",$message);
                $string = $t['member'][$i]['no'];
                if ($string[0] == 0) {
                    $phone = preg_replace('/0/', '62', $t['member'][$i]['no'], 1);
                } else {
                    $phone = $t['member'][$i]['no'];
                }
               
                
                $f = 'http://ginsms.gratika.id:1111/sendSms.do?username=valdo_inc&password=vald01234&senderid=GRATIKA&pesan='.$message_strip.'&msisdn='.$phone;
                $data = file_get_contents($f,TRUE);
         
                
                $e = array(
                    'status' => addslashes($data),
                    'sms_count' => $t['member'][$i]['sms_count']+1
                    );
                $where = array(
                    'no_hp' =>  $t['member'][$i]['no_hp']);
                $log = array(
                    'no_hp' => $t['member'][$i]['no_hp'],
                    'nama' => $t['member'][$i]['nama'],
                    'no_karyawan' => $t['member'][$i]['no_karyawan'],
                    'isi_pesan' => addslashes($message),
                    'type' =>'trans',
                    'sub_type' =>'',
                    'status' =>addslashes($data)
                    );
                $this->db->insert('td_pesan_send_log', $log);
                $this->db->update('td_member', $e, $where);
            }}
            }
            function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,'','.');
	return $hasil_rupiah;
 
}
          function sms_trans()
        {
         //echo 'a';
//            if('4297f44b13955235245b2497399d7a93'==$_GET['token'])
//            {
            $t['polis'] = $this->mpesan->td_get_trans();
            
            //var_dump( $t['polis'][0]['isi_pesan']);
            $t['member'] = $this->mpesan->td_get_member_trans();
            
//            var_dump($t['member']);
//            exit();
            if(count($t['member'])>0){
            for($i=0;$i<count($t['member']);$i++)
            {
                
                //$message = "Pemegang Polis Yth, Selamat bergabung Polis No ".$t['member'][$i]['no_polis']." telah disetujui. Hub CS 021-29023688 jika Anda belum menerima Polis dalam waktu 14 hari Terima kasih";
                $message =str_replace("[3]", $this->rupiah($t['member'][$i]['nominal_refund']),str_replace("[2]", $this->rupiah($t['member'][$i]['nominal_benar']),str_replace("[1]", $this->rupiah($t['member'][$i]['nominal_salah']), $t['polis'][0]['isi_pesan'])));
                $message_strip = str_replace(" ", "+",$message);
                $string = $t['member'][$i]['no'];
                if ($string[0] == 0) {
                    $phone = preg_replace('/0/', '62', $t['member'][$i]['no'], 1);
                } else {
                    $phone = $t['member'][$i]['no'];
                }

                var_dump($message);
                exit();
                
                
                $f = 'http://ginsms.gratika.id:1111/sendSms.do?username=valdo_inc&password=vald01234&senderid=GRATIKA&pesan='.$message_strip.'&msisdn='.$phone;
                $data = file_get_contents($f,TRUE);
         
                
                $e = array(
                    'status' => addslashes($data),
                    'sms_count' => $t['member'][$i]['sms_count']+1
                    );
                $where = array(
                    'no_hp' =>  $t['member'][$i]['no_hp']);
                $log = array(
                    'no_hp' => $t['member'][$i]['no_hp'],
                    'nama' => $t['member'][$i]['nama_karyawan'],
                    'no_karyawan' => $t['member'][$i]['no_karyawan'],
                    'isi_pesan' => addslashes($message),
                    'type' =>'Trans',
                    'sub_type' =>'',
                    'status' =>addslashes($data)
                    );
                $this->db->insert('td_pesan_send_log', $log);
                $this->db->update('td_member_trans', $e, $where);
            }}
            }

    
    
     public function upload_claim_received() {
        $a = array();
        $z = "WHERE sub_type='claim_received'";
        $b = "WHERE sub_type='claim_received' AND CHAR_LENGTH(status) > 3";
        $c = "WHERE sub_type='claim_received' AND status='1'";
        $d = "WHERE sub_type='claim_received' AND status='2'";
        $e = "WHERE sub_type='claim_received' AND status='5'";
        $f = "WHERE sub_type='claim_received' AND status='8'";
        $g = "WHERE sub_type='claim_received' AND status='7'";

        $a['total'] = $this->mpesan->get_all_member_received($z);
        $a['success'] = $this->mpesan->get_all_member_received($b);
        $a['invalid_number'] = $this->mpesan->get_all_member_received($c);
        $a['no_number'] = $this->mpesan->get_all_member_received($d);
        $a['ip_error'] = $this->mpesan->get_all_member_received($e);
        $a['new'] = $this->mpesan->get_all_member_received($f);
        $a['credit'] = $this->mpesan->get_all_member_received($g);
        
        $a['polis'] = $this->mpesan->get_claim_pay();
        $a['member'] = $this->mpesan->td_get_member();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
         $a['html']['css'] .= add_css('/bootstrap/css/alertify.core.css');

        $a['html']['css'] .= add_css('/bootstrap/css/alertify.default.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');

        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');

        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/alertify.js');

        $a['html']['js'] .= add_js('/bootstrap/js/dataTables.bootstrap.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/plugins/sparkline/jquery.sparkline.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');

        $a['html']['js'] .= add_js('/plugins/chartjs/Chart.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');

        //$id_group=$this->uri->segment(2);

        $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu', NULL, true);
        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, true);
         $a['pesan'] = '';
        if (isset($_POST["submit"])) {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    // echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($_FILES["file"]["name"])) {
                        unlink($_FILES["file"]["name"]);
                    }
                    $storagename = "assets/claim_received.xlsx";
                    move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
                    $inputFileName = 'assets/claim_received.xlsx';
                    try {
                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $arrayCount = count($allDataInSheet);
//                    var_dump($allDataInSheet[1]['A']);
//                    var_dump($allDataInSheet[2]['A']);
                  //  var_dump($allDataInSheet[3]['A']);
                    //var_dump($allDataInSheet[4]['A']);
                    //exit();
                    
                    if($allDataInSheet[1]['A']!=NULL)
                    {$mulai=2;
                    $da=$arrayCount-1;
                    }
                    else  if($allDataInSheet[2]['A']!=NULL)
                    {$mulai=3;
                    $da=$arrayCount-2;
                    }
                    else 
                    {$mulai=4;
                    $da=$arrayCount-3;
                    }
                    //echo $mulai;
                   //$no_polis ="0";
                    
                    for ($i = $mulai; $i <= $arrayCount; $i++) {
                        $header = $mulai - 1; 
                        if($allDataInSheet[$header]['A']=="No Polis")
                        {
                            $no_polis = trim($allDataInSheet[$i]["A"]);
                            $userMobile = trim($allDataInSheet[$i]["B"]);
//                            / var_dump($userMobile.$no_polis);
                          
                        }
                        else
                        {
                            $no_polis = trim($allDataInSheet[$i]["B"]);
                            $userMobile = trim($allDataInSheet[$i]["A"]);
                        }

                        
                        $query = $this->mpesan->get_claim_received_by_id(str_replace("#", "", $no_polis));
                        if ($query[0]['no_polis'] == "") 
                            {
                            $this->mpesan->upload_claim_received(str_replace("#", "", $no_polis), str_replace("#", "", $userMobile));
                        
                                $a['pesan'] ='<script>alert("You have Success Upload '.($da).' New Recieved Data")</script>
                          <strong><div class="alert alert-success btn-delay">
                          <strong></strong> Success! Upload  '. ($da).' new member
                        </div>';
                            } 
                        else {
                          //  echo '<script>alert("Duplicate Data!")</script>';
                        }
                    }
//                    $a['pesan'] ='<div class="alert alert-success">
//                    <strong></strong> You Have  '. (count($a['new'])).' new member
//                  </div>';
                
                }
            }
        } else {
          //  echo "No file selected <br />";
        }
        $this->load->view('pages/vupload_claim_received', $a, FALSE);
    }
    public function upload_hari_raya_imlek() {
        $a = array();
        $z = "WHERE sub_type='imlek'";
        $b = "WHERE sub_type='imlek' AND CHAR_LENGTH(status) > 3";
        $c = "WHERE sub_type='imlek' AND status='1'";
        $d = "WHERE sub_type='imlek' AND status='2'";
        $e = "WHERE sub_type='imlek' AND status='5'";
        $f = "WHERE sub_type='imlek' AND status='8'";
        $g = "WHERE sub_type='imlek' AND status='7'";

        $a['total'] = $this->mpesan->get_all_member_imlek($z);
        $a['success'] = $this->mpesan->get_all_member_imlek($b);
        $a['invalid_number'] = $this->mpesan->get_all_member_imlek($c);
        $a['no_number'] = $this->mpesan->get_all_member_imlek($d);
        $a['ip_error'] = $this->mpesan->get_all_member_imlek($e);
        $a['new'] = $this->mpesan->get_all_member_imlek($f);
        $a['credit'] = $this->mpesan->get_all_member_imlek($g);
//        
//        $a['polis'] = $this->mpesan->get_all_member_imlek();
//        $a['member'] = $this->mpesan->td_get_member();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
         $a['html']['css'] .= add_css('/bootstrap/css/alertify.core.css');

        $a['html']['css'] .= add_css('/bootstrap/css/alertify.default.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');

        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');

        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/alertify.js');

        $a['html']['js'] .= add_js('/bootstrap/js/dataTables.bootstrap.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/plugins/sparkline/jquery.sparkline.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');

        $a['html']['js'] .= add_js('/plugins/chartjs/Chart.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');

        //$id_group=$this->uri->segment(2);

        $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu', NULL, true);
        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, true);
        $a['pesan'] = '';
        if (isset($_POST["submit"])) {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    // echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($_FILES["file"]["name"])) {
                        unlink($_FILES["file"]["name"]);
                    }
                    $storagename = "assets/imlek.xlsx";
                    move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
                    $inputFileName = 'assets/imlek.xlsx';
                    try {
                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $arrayCount = count($allDataInSheet);
                    
                    $mulai=2;
                    $da=$arrayCount-1;
                    
                    //echo $mulai;
                   //$no_polis ="0";
                    
                    for ($i = $mulai; $i <= $arrayCount; $i++) {
                        $header = $mulai - 1; 
//                        if($allDataInSheet[$header]['A']=="No Polis")
//                        {
                            $no_polis = trim($allDataInSheet[$i]["B"]);
                            $jenis_kelamin = trim($allDataInSheet[$i]["C"]);
                            $nama_pemegang = trim($allDataInSheet[$i]["D"]);
                            $agama = trim($allDataInSheet[$i]["E"]);
                            $nama_produk= trim($allDataInSheet[$i]["F"]);
                            $no_hp = trim($allDataInSheet[$i]["G"]);
//                        }
//                        else
//                        {
//                            $no_polis = trim($allDataInSheet[$i]["B"]);
//                            $userMobile = trim($allDataInSheet[$i]["A"]);
//                        }

                        
                        $query = $this->mpesan->get_imlek_by_id(str_replace("#", "", $no_polis));
                        if ($query[0]['no_polis'] == "") 
                            {
                            $this->mpesan->upload_imlek(str_replace("#", "", $no_polis),$jenis_kelamin,$nama_pemegang,$agama,$nama_produk, str_replace("#", "", $no_hp));
                        
                                $a['pesan'] ='<script>alert("You have Success Upload New Imlek Data")</script>
                                    <strong><div class="alert alert-success btn-delay">
                                    <strong></strong> Success! Upload new member
                                  </div>';
                            } 
                        else {
                            //echo '<script>alert("Duplicate Data!")</script>';
                        }
                    }
//                    $a['pesan'] ='<div class="alert alert-success">
//                    <strong></strong> You Have  '. (count($a['new'])).' new member
//                  </div>';
                
                }
            }
        } else {
          //  echo "No file selected <br />";
        }
        $this->load->view('pages/vupload_imlek', $a, FALSE);
    }
}
