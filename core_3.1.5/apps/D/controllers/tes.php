<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tes extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('tess');
    }
    
    function index() {
//     $key = hash('sha256',md5(sha1(md5("valdo-sms-bls-key"))));
//        if($_GET['key'] != $key){redirect(base_url());}
        $visitor['post'] = $this->tess->get_log();
        echo json_encode($visitor);

    }
    
    
}