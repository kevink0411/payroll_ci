<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'weburi', 'header', 'file', 'contentdate', 'stringurl', 'textlimiter', 'security'));
        $this->load->library('form_validation');
        $this->load->model(array('mhome', 'mpesan','mpayrol'));
        $this->is_logged_in();
    }

    function index() {
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/datatables/dataTables.bootstrap.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');

        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery-ui.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/plugins/datatables/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/plugins/datatables/dataTables.bootstrap.min.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-confirm-delete.js');
        $a['html']['js'] .= add_js('/bootstrap/js/test.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');

        //$id_group=$this->uri->segment(2);       
        // $a['member'] = $this->mpesan->td_get_member();
        //$a['member'] = $this->mpesan->td_get_member();
         //$a['list'] = $this->mpayrol->td_get_member();
        $a['list'] =$this->mpayrol->getmonitordata();
        
        $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu', NULL, true);
        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
        $a['template']['header'] = $this->load->view('template/vheader', NULL, true);
        
        $this->load->view('pages/vmonitoring', $a, FALSE);
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        redirect('login');
    }
    function delete_all()
    {
        $this->mpayrol->delete_all();
        redirect('monitoring');
    }

    function delete_by_id()
    {
        $this->mpayrol->delete_all_by_id();
        redirect('monitoring');
    }
}