<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_payrol extends CI_Controller {

	public function __construct()
	{
            parent::__construct();
            $this->load->helper(array('url','weburi','header','file','contentdate','stringurl','textlimiter','security','dropdown'));
            $this->load->model(array('mpayrol','admin_log'));
            $this->load->library('form_validation');
            $this->is_logged_in();
	}
        
	public function index()
	{
        $a = array();        
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
        $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
        $a['html']['css'] .= add_css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
        $a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');
        $a['html']['css'] .= add_css('/bootstrap/css/jquery.datetimepicker.css');
        $a['html']['css'] .= add_css('/plugins/datatables/dataTables.bootstrap.css');
  
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
        //$a['html']['js'] = add_js('/monitoring/jquery.js');
        $a['html']['js'] .= add_js('/monitoring/jquery.datatable.js');             
        $a['html']['js'] .= add_js('/monitoring/datatable.bootstrap.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/dataTables.bootstrap.js');
        $a['html']['js'] .= add_js('/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/plugins/sparkline/jquery.sparkline.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
        $a['html']['js'] .= add_js('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        $a['html']['js'] .= add_js('/plugins/slimScroll/jquery.slimscroll.min.js');
        
        $a['html']['js'] .= add_js('/plugins/chartjs/Chart.min.js');
        $a['html']['js'] .= add_js('/dist/js/demo.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');
        $a['html']['js'] .= add_js('/bootstrap/js/jquery.datetimepicker.full.min.js');
                
//                $a['html']['css'] = add_css('/bootstrap/css/bootstrap.min.css');
////                $a['html']['css'] .= add_css('/monitoring/datatable.bootstrap.min.css');
//                $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
////                $a['html']['css'] .= add_css('/plugins/datatables/dataTables.bootstrap.css');
//                $a['html']['css'] .= add_css('/bootstrap/css/font-awesome.min.css');
//                $a['html']['css'] .= add_css('/bootstrap/css/ionicons.min.css');
//                $a['html']['css'] .= add_css('/dist/css/AdminLTE.min.css');
//                $a['html']['css'] .= add_css('/dist/css/skins/_all-skins.min.css');
//                 $a['html']['css'] .= add_css('/bootstrap/css/jquery.datetimepicker.css');
//
//                $a['html']['js'] = add_js('/monitoring/jquery.js');
//                $a['html']['js'] .= add_js('/monitoring/jquery.datatable.js');
//                $a['html']['js'] .= add_js('/monitoring/datatable.bootstrap.js');
//              $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-confirm-delete.js');
//              $a['html']['js'] .= add_js('/bootstrap/js/jquery.datetimepicker.full.min.js');
//              
               // $a['html']['js'] .= add_js('/bootstrap/js/test.js');
               // $a['html']['js'] .= add_js('/bootstrap/js/bootstrap-datepicker.min.js');
                //$a['html']['js'] = add_js('/plugins/jQuery/jQuery-2.2.0.min.js');
               // $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.min.js');
//                /$a['html']['js'] .= add_js('/bootstrap/js/jquery-ui.min.js');
                //$a['html']['js'] .= add_js('/monitoring/jquery.js');
               
                //$a['html']['js'] .= add_js('/plugins/datatables/jquery.dataTables.min.js');
                //$a['html']['js'] .= add_js('/plugins/datatables/dataTables.bootstrap.min.js');
               
                //$a['html']['js'] .= add_js('/monitoring/jquery.datatable.js');
                
             
                
                
                $a['list'] = $this->mpayrol->td_get_member();
                $a['bank'] = $this->mpayrol->getbank();
                $a['bulan'] = $this->mpayrol->getbulan();
                $a['client'] = $this->mpayrol->getclient();
                //$a['member'] = $this->mpayrol->td_get_member();
                $a['template']['sidebarmenu'] = $this->load->view('template/vsidebarmenu',NULL,true);
                $a['template']['footer'] = $this->load->view('template/vfooter',NULL,true);
                $a['template']['header'] = $this->load->view('template/vheader',NULL,true);
            
             $prov_id = $this->uri->segment(4,0);
        if ($prov_id != "") {
            $a['dropdownprov'] = $this->mpayrol->cms_list_bulan();
            //$a['dropdowncity'] = $this->mpayrol->cms_list_all_bulan();
            $a['dropdowncity'] = $this->mpayrol->cms_list_all_klien($prov_id);
           
           $this->load->view('pages/vlist_payrol',$a,FALSE);
        } else {
            $a['dropdownprov'] = $this->mpayrol->cms_list_bulan();
            $a['dropdowncity'] = $this->mpayrol->cms_list_all_klien();
        $this->load->view('pages/vlist_payrol',$a,FALSE);
        }    
                
           // $this->load->view('pages/vlist_payrol',$a,FALSE);
	}
	
        public function ajax_list()
	{
		$list = $this->mpayrol->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $admin){
			$no++;
			$row = array();
			$row[] = $admin->nama;
            $row[] = $admin->nik;
			$row[] = $admin->payment_date;
			$row[] = '<a data-toggle="tooltip" class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_payrol('."'".$admin->id_payrol."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a data-toggle="tooltip" class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_payrol('."'".$admin->id_payrol."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
			$data[] = $row;}
		$output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mpayrol->count_all(),
                "recordsFiltered" => $this->mpayrol->count_filtered(),
                "data" => $data,);
		echo json_encode($output);
	}
	public function ajax_edit($id)
	{
            $data = $this->mpayrol->get_by_id($id);
            $data->id_payrol =  $data->id_payrol;
            echo json_encode($data);
	}
        
    public function ajax_call() 
    {
        $parent_cat = $this->input->post('bulan');
        $data['sc_get_par'] = $this->mpayrol->cms_list_subcat($parent_cat);
        $sc = json_encode($data['sc_get_par']);
        echo $sc;
    } 

    public function ajax_call_prov() 
    {
        $provinsi_id = $this->input->post('bulan');
        $data['sc_get_prov'] = $this->mpayrol->cms_list_klien($provinsi_id);
        $sc = json_encode($data['sc_get_prov']);
        echo $sc;
    }
        
	public function ajax_add()
	{
            $this->_validate();
            $data = array(
                'isi_pesan' => addslashes($this->input->post('isi_pesan')),
                'jenis_hari_raya' => $this->input->post('jenis_hari_raya'),
                'tanggal_hari_raya' =>  $this->input->post('tanggal_hari_raya'),
            );
            $this->mpayrol->save($data);
            $this->admin_log->log_add_hari_raya();
            echo json_encode(array("status" => TRUE));
	}
    

	public function ajax_update()
	{
            $this->_validate();
            $data = array(
                'gaji_pokok' => addslashes($this->input->post('gaji_pokok')),
                'rapel' =>  $this->input->post('rapel'),
                'transport_tetap' => $this->input->post('transport_tetap'),
                'transport_variable' => $this->input->post('transport_variable'),
                'makan_variable' => $this->input->post('makan_variable'),
                'lembur1' => $this->input->post('lembur1'),
                'lembur2' => $this->input->post('lembur2'),
                'total_lembur' => $this->input->post('total_lembur'),
                'shift1' => $this->input->post('shift1'),
                'shift2' => $this->input->post('shift2'),
                'total_shift' => $this->input->post('total_shift'),
                'insentif1' => $this->input->post('insentif1'),
                'insentif2' => $this->input->post('insentif2'),
                'insentif3' => $this->input->post('insentif3'),
                'total_insentif' => $this->input->post('total_insentif'),
                'bonus' => $this->input->post('bonus'),
                'thr' => $this->input->post('thr'),
                'lain_lain1' => $this->input->post('lain_lain1'),
                'lain_lain2' => $this->input->post('lain_lain2'),
                'lain_lain3' => $this->input->post('lain_lain3'),
                'total_lain_lain' => $this->input->post('total_lain_lain'),
                'gross' => $this->input->post('gross'),
                'jamsostek_karyawan' => $this->input->post('jamsostek_karyawan'),
                'jamsostek_perusahaan' => $this->input->post('jamsostek_perusahaan'),
                'pensiun_karyawan' => $this->input->post('pensiun_karyawan'),
                'pensiun_perusahaan' => $this->input->post('pensiun_perusahaan'),
                'bpjs_karyawan' => $this->input->post('bpjs_karyawan'),
                'bpjs_perusahaan' => $this->input->post('bpjs_perusahaan'),
                'pph21' => $this->input->post('pph21'),
                'thp' => $this->input->post('thp'),
                'pot_pinjam' => $this->input->post('pot_pinjam'),
                'pot_lain' => $this->input->post('pot_lain'),
                'total_pot' => $this->input->post('total_pot'),

            );
            $this->mpayrol->update(array('id_payrol' => $this->input->post('id_payrol')), $data);
            $this->admin_log->log_edit_hari_raya();
            echo json_encode(array("status" => TRUE));
	}
        
	public function ajax_delete($id)
	{
            $this->mpayrol->delete_by_id($id);
            echo json_encode(array("status" => TRUE));
	}
        
       
	private function _validate()
	{
            $data = array();
            $data['error_string'] = array();
            $data['inputerror'] = array();
            $data['status'] = TRUE;

            
            if($this->input->post('gaji_pokok') == '')
            {
                    $data['inputerror'][] = 'GAJI POKOK';
                    $data['error_string'][] = '';
                    $data['status'] = FALSE;
            }
             
            //  if($this->input->post('jenis_hari_raya') == '')
            // {
            //         $data['inputerror'][] = 'jenis_hari_raya';
            //         $data['error_string'][] = 'Jenis Hari Raya is required';
            //         $data['status'] = FALSE;
            // }
            //  if($this->input->post('tanggal_hari_raya') == '')
            // {
            //         $data['inputerror'][] = 'tanggal_hari_raya';
            //         $data['error_string'][] = 'Tanggal Hari Raya is required';
            //         $data['status'] = FALSE;
            // }
            if($data['status'] === FALSE)
            {
                    echo json_encode($data);
                    exit();
            }
	} 
    function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('');
        }
    }
}