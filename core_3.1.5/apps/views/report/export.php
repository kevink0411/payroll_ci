<html xmlns:o="urn:schemas-microsoft-com:office:office" 
      xmlns:x="urn:schemas-microsoft-com:office:excel" 
      xmlns="http://www.w3.org/TR/REC-html40">
    <head><style>
            .s1 {mso-number-format:"#,##0.00_ ;[Red]-#,##0.00";}
            .s2 {mso-number-format:"0";}
        </style></head>
    <?php
    header("Content-type: application/octet-stream");
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=EXPORT-PAYROL.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    ?>
    <div>
    </div>
    <?php
    $total = count($alldata);
    echo"
    <table border='1'>
        <tbody>
        <tr>
        <th colspan='29' style='background-color:#adad85;font-size: 22;'>DATA PAYROL</th>  
         </tr>
        <tr>
        <th>NIK</th>
        <th>NAMA</th>
        <th>GAJI_POKOK</th>
        <th>RAPEL</th>
        <th>TRANSPORT_TETAP</th>
        <th>TRANSPORT_VARIABLE</th>
        <th>MAKAN_VARIABLE</th> 
        <th>TOTAL_LEMBUR</th>
        <th>TOTAL SHIFT</th>
        <th>TOTAL_INSENTIF</th>
        <th>BONUS</th>
        <th>THR</th>
        <th>TOTAL_LAIN_LAIN</th>
        <th>GROSS</th>
        <th>JAMSOSTEK_KARYAWAN</th>
        <th>JAMSOSTEK_PERUSAHAAN</th>
        <th>PENSIUN_KARYAWAN</th>
        <th>PENSIUN_PERUSAHAAN</th>
        <th>BPJS_KARYAWAN</th>
        <th>BPJS_PERUSAHAAN</th>
        <th>PPH21</th>
        <th>THP</th>
        <th>KLIEN</th>
        <th>BULAN</th>
        <th>REKENING</th>
        <th>BANK</th>
		<th>PAYMENT_DATE</th>
        <th>KETERANGAN</th>
        <th>HRD</th>
      </tr>";

    for ($i = 0; $i < $total; $i++) {
        echo "<tr  align='center'>
                    <td>" . $alldata[$i]['nik'] . "</td>
                    <td>" . $alldata[$i]['nama'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_GAJI'] . "'>" . $alldata[$i]['SUM_GAJI'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_RAPEL'] . "'>" . $alldata[$i]['SUM_RAPEL'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TRANSPORT_TETAP'] . "'>" . $alldata[$i]['SUM_TRANSPORT_TETAP'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TRANSPORT_VARIABLE'] . "'>" . $alldata[$i]['SUM_TRANSPORT_VARIABLE'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_MAKAN_VARIABLE'] . "'>" . $alldata[$i]['SUM_MAKAN_VARIABLE'] . "</td>   
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TOTAL_LEMBUR'] . "'>" . $alldata[$i]['SUM_TOTAL_LEMBUR'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TOTAL_SHIFT'] . "'>" . $alldata[$i]['SUM_TOTAL_SHIFT'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TOTAL_INSENTIF'] . "'>" . $alldata[$i]['SUM_TOTAL_INSENTIF'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_BONUS'] . "'>" . $alldata[$i]['SUM_BONUS'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_THR'] . "'>" . $alldata[$i]['SUM_THR'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_TOTAL_LAIN_LAIN'] . "'>" . $alldata[$i]['SUM_TOTAL_LAIN_LAIN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_GROSS'] . "'>" . $alldata[$i]['SUM_GROSS'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_JAMSOSTEK_KARYAWAN'] . "'>" . $alldata[$i]['SUM_JAMSOSTEK_KARYAWAN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_JAMSOSTEK_PERUSAHAAN'] . "'>" . $alldata[$i]['SUM_JAMSOSTEK_PERUSAHAAN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_PENSIUN_KARYAWAN'] . "'>" . $alldata[$i]['SUM_PENSIUN_KARYAWAN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_PENSIUN_PERUSAHAAN'] . "'>" . $alldata[$i]['SUM_PENSIUN_PERUSAHAAN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_BPJS_KARYAWAN'] . "'>" . $alldata[$i]['SUM_BPJS_KARYAWAN'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_BPJS_PERUSAHAAN'] . "'>" . $alldata[$i]['SUM_BPJS_PERUSAHAANB'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_PPH21'] . "'>" . $alldata[$i]['SUM_PPH21'] . "</td>
                    <td class='s1' x:num='" . $alldata[$i]['SUM_THP'] . "'>" . $alldata[$i]['SUM_THP'] . "</td>
                    <td>" . $alldata[$i]['klien'] . "</td>
                    <td>" . $alldata[$i]['bulan'] . "</td>
                    <td class='s2' x:num='". $alldata[$i]['no_rekening']."'>".$alldata[$i]['no_rekening']."</td>
                    <td>" . $alldata[$i]['bank'] . "</td>
					<td>" . $alldata[$i]['payment_date'] . "</td>
                    <td>" . $alldata[$i]['keterangan'] . "</td>
					<td>" . $alldata[$i]['hrd'] . "</td>
            </tr>";
    }
    echo "<tr style='background-color:#adad85;' >
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GAPOK'] . "'>" . $alldata[0]['TOTAL_GAPOK'] . "</td>
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_RAPEL'] . "'>" . $alldata[0]['TOTAL_RAPEL'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_TETAP'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_VARIABLE'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_MAKAN_VARIABLE'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_MAKAN_VARIABLE'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LEMBUR'] . "'>" . $alldata[0]['TOTAL_LEMBUR'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_SHIFT'] . "'>" . $alldata[0]['TOTAL_SHIFT'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_INSENTIF'] . "'>" . $alldata[0]['TOTAL_INSENTIF'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_TRANSPORT_BONUS'] . "'>" . $alldata[0]['TOTAL_TRANSPORT_BONUS'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THR'] . "'>" . $alldata[0]['TOTAL_THR'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_LAIN_LAIN'] . "'>" . $alldata[0]['TOTAL_LAIN_LAIN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GROSS'] . "'>" . $alldata[0]['TOTAL_GROSS'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PPH21'] . "'>" . $alldata[0]['TOTAL_PPH21'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_THP'] . "'>" . $alldata[0]['TOTAL_THP'] . "</td> 
          
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center'>-</td>
		    <td align='center'>-</td> 
           <td align='center'>-</td>
		   <td align='center'>-</td>
         </tr>";
    echo "</tbody></table>";
    