<html xmlns:o="urn:schemas-microsoft-com:office:office" 
      xmlns:x="urn:schemas-microsoft-com:office:excel" 
      xmlns="http://www.w3.org/TR/REC-html40">
    <head><style>
        .s1 {mso-number-format:"#,##0.00_ ;[Red]-#,##0.00";}
        .s2 {mso-number-format:"0";}
    </style></head>
<?php

header("Content-type: application/octet-stream");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=EXPORT-PAJAK.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php
$no =1;
$total = count($alldata);
echo"
    <table border='1'>
        <tbody>
        <tr>
        <th colspan='30' style='font-size: 22; background-color:#a3a375'>DATA PAYROL</th>  
         </tr>
        <tr>
        <th>NO</th>
        <th>NIK</th>
        <th>NAMA</th>
        <th>TANGGAL MASUK</th>
        <th>TANGGAL_KELUAR</th>
        <th>TANGGAL RESIGN PAJAK</th>
        <th>STATUS PAJAK</th>
        <th>JENIS KELAMIN</th>
        <th>DEPT</th>
        <th>JABATAN</th> 
        <th>CABANG</th>
        <th>STATUS KARYAWAN</th>
        <th>NPWP</th>
        <th>NO KTP</th>
        <th>ALAMAT</th>
        <th>GAJI POKOK</th>
        <th>RAPEL</th>
        <th>TOTAL_INSENTIF</th>
        <th>GROSS</th>
        <th>JAMSOSTEK_KARYAWAN</th>
        <th>JAMSOSTEK PERUSAHAAN</th>
        <th>PENSIUN KARYAWAN</th>
		<th>PENSIUN PERUSAHAAN</th>
        <th>BPJS KARYAWAN</th>
        <th>BPJS PERUSAHAAN</th>
        <th>PPH21</th>
        <th>ADDITIONAL AFTER TAX</th>
        <th>BULAN</th>
        <th>KETERANGAN</th>
      </tr>";
        for ($i = 0; $i<$total; $i++)
        {  
       echo "<tr align='center'>
                    <td>".$no."</td>
                    <td class='s2' x:num='". $alldata[$i]['nik']."'>".$alldata[$i]['nik']."</td>
                    <td>".$alldata[$i]['nama']."</td>
                    <td>".$alldata[$i]['tgl_masuk']."</td>
                    <td>".$alldata[$i]['tgl_keluar']."</td>
                    <td>-</td>
                    <td>".$alldata[$i]['status_pajak']."</td>
                    <td>".$alldata[$i]['jns_kelamin']."</td>
                    <td>".$alldata[$i]['dept']."</td>
                    <td>".$alldata[$i]['jabatan']."</td>  
                    <td>".$alldata[$i]['cabang']."</td>
                    <td>".$alldata[$i]['status_pegawai']."</td>
                    <td class='s2' x:num='". $alldata[$i]['no_npwp']."'>".$alldata[$i]['no_npwp']."</td>
                    <td class='s2' x:num='". $alldata[$i]['no_ktp']."'>".$alldata[$i]['no_ktp']."</td>
                    <td>".$alldata[$i]['alamat_rumah']."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_GAJI']."'>". $alldata[$i]['SUM_GAJI'] ."</td>
                    <td class='s1' x:num='". ($alldata[$i]['SUM_RAPEL']+$alldata[$i]['SUM_RAPEL']+$alldata[$i]['transport_variable']+$alldata[$i]['SUM_LEMBUR']+$alldata[$i]['SUM_LEMBUR']+$alldata[$i]['total_lain_lain'])."'>".($alldata[$i]['SUM_TRANSPORT_TETAP']+$alldata[$i]['SUM_TRANSPORT_TETAP']+$alldata[$i]['SUM_TRANSPORT_VARIABLE']+$alldata[$i]['SUM_TOTAL_LEMBUR']+$alldata[$i]['SUM_TOTAL_SHIFT']+$alldata[$i]['SUM_TOTAL_LAIN_LAIN'])."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_TOTAL_INSENTIF']."'>". $alldata[$i]['SUM_TOTAL_INSENTIF'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_GROSS']."'>". $alldata[$i]['SUM_GROSS'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_JAMSOSTEK_KARYAWAN']."'>". $alldata[$i]['SUM_JAMSOSTEK_KARYAWAN'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_JAMSOSTEK_PERUSAHAAN']."'>". $alldata[$i]['SUM_JAMSOSTEK_PERUSAHAAN'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_PENSIUN_KARYAWAN']."'>". $alldata[$i]['SUM_PENSIUN_KARYAWAN'] ."</td>
					<td class='s1' x:num='". $alldata[$i]['SUM_PENSIUN_PERUSAHAAN']."'>". $alldata[$i]['SUM_PENSIUN_PERUSAHAAN'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_BPJS_KARYAWAN']."'>". $alldata[$i]['SUM_BPJS_KARYAWAN'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_BPJS_PERUSAHAAN']."'>". $alldata[$i]['SUM_BPJS_PERUSAHAAN'] ."</td>
                    <td class='s1' x:num='". $alldata[$i]['SUM_PPH21']."'>". $alldata[$i]['SUM_PPH21'] ."</td>
                    <td>0</td>
                    <td>".$alldata[$i]['bulan']."</td>
                    <td>".$alldata[$i]['keterangan']."</td>
            </tr>";
       $no++;
        }
         echo "<tr style='background-color:#adad85;' >
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td>
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GAJI_POKOK'] . "'>" . $alldata[0]['TOTAL_GAJI_POKOK'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_RAPEL'] . "'>" . $alldata[0]['TOTAL_RAPEL'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_INSENTIF'] . "'>" . $alldata[0]['TOTAL_INSENTIF'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_GROSS'] . "'>" . $alldata[0]['TOTAL_GROSS'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_JAMSOSTEK_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_KARYAWAN'] . "</td> 
		   <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_PENSIUN_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "'>" . $alldata[0]['TOTAL_BPJS_KARYAWAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "'>" . $alldata[0]['TOTAL_BPJS_PERUSAHAAN'] . "</td> 
           <td align='center' class='s1' x:num='" . $alldata[0]['TOTAL_PPH21'] . "'>" . $alldata[0]['TOTAL_PPH21'] . "</td> 
           <td align='center'>-</td> 
           <td align='center'>-</td> 
           <td align='center'>-</td>
           <td align='center'>-</td> 
         </tr>";
        
echo "</tbody></table>";