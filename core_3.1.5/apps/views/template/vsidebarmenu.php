<div class="user-panel">
    <div class="pull-left image">
        <img src="<?php echo base_url() ?>assets/dist/img/admin-button-icon-md.png" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p><?php echo $this->session->userdata('nama_lengkap') ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<ul class="sidebar-menu" id="leftcolumn" >
    <li class="header">MAIN NAVIGATION</li>
<!--    <li><a href="<?php echo base_url() ?>upload"><i class="fa fa-upload"></i>UPLOAD</a></li>-->
<!--    <li class="treeview">
        <a href="#">
            <i class="fa fa-upload"></i>
            <span>UPLOAD</span><span class="label label-primary pull-right"></span></a>
        <ul class="treeview-menu collapsed">
            <li><a href="<?php echo base_url() ?>upload-polis"><i class="fa fa-user"></i>UPLOAD DATA</a></li>
            <li><a href="<?php echo base_url() ?>upload-claim-payment"><i class="fa fa-user"></i>UPLOAD DATA TRANSVIS</a></li>
            <li><a href="<?php echo base_url() ?>upload-claim-received"><i class="fa fa-user"></i>CLAIM RECEIVED</a></li>
            <li><a href="<?php echo base_url() ?>upload-imlek"><i class="fa fa-user"></i>HARI RAYA IMLEK</a></li>
        </ul>
    </li>-->
    
      <li class="treeview">
        <a href="#">
            <i class="fa fa-address-book"></i>
            <span>DATA</span><span class="label label-primary pull-right"></span></a>
         <ul class="treeview-menu collapsed">
            <li><a href="<?php echo base_url() ?>payrol-data"><i class="fa fa-user"></i>LIST PAYROL</a></li>
            <!--<li><a href="<?php echo base_url() ?>upload-claim-payment"><i class="fa fa-user"></i>UPLOAD DATA TRANSVIS</a></li>-->
<!--            <li><a href="<?php echo base_url() ?>upload-claim-received"><i class="fa fa-user"></i>CLAIM RECEIVED</a></li>
            <li><a href="<?php echo base_url() ?>upload-imlek"><i class="fa fa-user"></i>HARI RAYA IMLEK</a></li>-->
        </ul>
    </li>
 <?php if($this->session->userdata('id_admin') ==1 OR $this->session->userdata('id_admin') ==10){ ?>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-circle"></i>
            <span>Monitoring</span><span class="label label-primary pull-right"></span></a>
         <ul class="treeview-menu collapsed">
            <li><a href="<?php echo base_url() ?>monitoring"><i class="fa fa-user"></i>Monitoring</a></li>
            <!--<li><a href="<?php echo base_url() ?>upload-claim-payment"><i class="fa fa-user"></i>UPLOAD DATA TRANSVIS</a></li>-->
<!--            <li><a href="<?php echo base_url() ?>upload-claim-received"><i class="fa fa-user"></i>CLAIM RECEIVED</a></li>
            <li><a href="<?php echo base_url() ?>upload-imlek"><i class="fa fa-user"></i>HARI RAYA IMLEK</a></li>-->
        </ul>
    </li>

       <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i>
            <span>USER</span><span class="label label-primary pull-right"></span></a>
         <ul class="treeview-menu collapsed">
            <li><a href=""><i class="fa fa-user"></i>USER</a></li>
            <!--<li><a href="<?php echo base_url() ?>upload-claim-payment"><i class="fa fa-user"></i>UPLOAD DATA TRANSVIS</a></li>-->
<!--            <li><a href="<?php echo base_url() ?>upload-claim-received"><i class="fa fa-user"></i>CLAIM RECEIVED</a></li>
            <li><a href="<?php echo base_url() ?>upload-imlek"><i class="fa fa-user"></i>HARI RAYA IMLEK</a></li>-->
        </ul>
    </li>
  <?php } ?>
 
    
    <?php if($this->session->userdata('id_admin')!=2){ ?>
<!--    <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>Template</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
        <li><a href="javascript:void(0)"  onclick="link_polis()"><i class="fa fa-user"></i>TEMPLATE MANDIRI</a></li>
        <li><a href="javascript:void(0)"  onclick="link_spaj()"><i class="fa fa-user"></i>TEMPLATE TRANSVISION</a></li>
             <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>Ucapan</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>1" ><i class="fa fa-user"></i>Hari Raya</a></li>
            <li><a href="javascript:void(0)"  onclick="link_ultah()"><i class="fa fa-user"></i>Ultah</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>POS</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="javascript:void(0)"  onclick="alamat()"><i class="fa fa-user"></i>Alamat</a></li>
            <li><a href="javascript:void(0)"  onclick="aro()"><i class="fa fa-user"></i>Aro</a></li>
            <li><a href="javascript:void(0)"  onclick="mature()"><i class="fa fa-user"></i>Mature</a></li>
            <li><a href="javascript:void(0)"  onclick="ahli_waris()"><i class="fa fa-user"></i>Ahli Waris</a></li>
            <li><a href="javascript:void(0)"  onclick="no_rek()"><i class="fa fa-user"></i>No Rek</a></li>
            <li><a href="javascript:void(0)"  onclick="link_surender()"><i class="fa fa-user"></i>Surender</a></li>
            <li><a href="javascript:void(0)"  onclick="link_marketing()"><i class="fa fa-user"></i>Marketing</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>Unit Link</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="javascript:void(0)"  onclick="link_freelook()"><i class="fa fa-user"></i>Freelook</a></li>
            <li><a href="javascript:void(0)"  onclick="link_switching()"><i class="fa fa-user"></i>Switching</a></li>
            <li><a href="javascript:void(0)"  onclick="link_topup()"><i class="fa fa-user"></i>Top Up</a></li>
            <li><a href="javascript:void(0)"  onclick="link_lapsed()"><i class="fa fa-user"></i>Lapsed</a></li>
            <li><a href="javascript:void(0)"  onclick="link_reinstat()"><i class="fa fa-user"></i>Reinstat Ment</a></li>
            <li><a href="javascript:void(0)"  onclick="link_surender_unit()"><i class="fa fa-user"></i>Surender</a></li>
            <li><a href="javascript:void(0)"  onclick="link_withdraw()"><i class="fa fa-user"></i>Withdraw</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>Claim</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="javascript:void(0)"  onclick="link_claim_pay()"><i class="fa fa-user"></i>Payment</a></li>
            <li><a href="javascript:void(0)"  onclick="link_claim_pending()"><i class="fa fa-user"></i>Pending</a></li>
            <li><a href="javascript:void(0)"  onclick="link_claim_received()"><i class="fa fa-user"></i>Received</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-comments"></i>
            <span>Billing</span>
            <span class="label label-primary pull-right"></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="javascript:void(0)" onclick="link_bill_pay()"><i class="fa fa-user"></i>Payment</a></li>
            </ul>
        
    </li>
        </ul>
    </li>-->
   <?php }?>
<!--     <li class="treeview">
        <a href="#">
            <i class="fa fa-book"></i>
            <span>REPORT</span><span class="label label-primary pull-right"></span></a>
       
    </li>-->
<!--    <li class="treeview">
        <a href="<?php echo base_url() ?>logout">
            <i class="fa fa-user-times"></i>
            <span class="text-light-blue">Logout</span>
            <span class="label label-primary pull-right"></span>
        </a>
    </li>-->
</ul>

