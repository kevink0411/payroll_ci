

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mpesan extends CI_Model {

    var $table = 'td_pesan';
    var $column_order = array('isi_pesan', null);
    var $column_search = array('isi_pesan');
    var $order = array('id_pesan' => 'desc');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $this->db->from($this->table);
        $this->db->limit('100');
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id) {
        $this->db->from($this->table);
        $this->db->where('id_pesan', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id) {
        $this->db->where('id_pesan', $id);
        $this->db->delete($this->table);
    }
    
    function td_get_spaj() {
        $sql = "SELECT * FROM td_pesan where sub_type ='spaj'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_polis() {
        $sql = "SELECT * FROM td_pesan where sub_type ='polis'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_trans() {
        $sql = "SELECT * FROM td_pesan where sub_type ='trans'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_ultah() {
        $sql = "SELECT * FROM td_pesan WHERE sub_type ='ultah'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_ahli_waris() {
        $sql = "SELECT * FROM td_pesan where sub_type ='ahli_waris'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_alamat() {
        $sql = "SELECT * FROM td_pesan where sub_type ='alamat'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function td_get_aro() {
        $sql = "SELECT * FROM td_pesan where sub_type ='aro'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_marketing() {
        $sql = "SELECT * FROM td_pesan where sub_type ='marketing'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function td_get_mature() {
        $sql = "SELECT * FROM td_pesan where sub_type ='mature'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function td_get_no_rek() {
        $sql = "SELECT * FROM td_pesan where sub_type ='no_rek'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_surender() {
        $sql = "SELECT * FROM td_pesan where sub_type ='surender'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_surender_unit() {
        $sql = "SELECT * FROM td_pesan where type ='unit_link' AND sub_type ='surender'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_freelook() {
        $sql = "SELECT * FROM td_pesan where sub_type ='freelook'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_lapsed() {
        $sql = "SELECT * FROM td_pesan where sub_type ='lapsed'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_reinstat() {
        $sql = "SELECT * FROM td_pesan where sub_type ='reinstat'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_switching() {
        $sql = "SELECT * FROM td_pesan where sub_type ='switching'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_topup() {
        $sql = "SELECT * FROM td_pesan where sub_type ='topup'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_withdraw() {
        $sql = "SELECT * FROM td_pesan where sub_type ='withdraw'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_hari_raya() {
        $sql = "SELECT * FROM td_pesan where sub_type ='hari_raya'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_pay_bill() {
        $sql = "SELECT * FROM td_pesan WHERE type='billing' AND sub_type ='payment'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_pay_claim() {
        $sql = "SELECT * FROM td_pesan WHERE type='claim' AND sub_type ='payment'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
//    function td_get_imlek_template() {
//        $sql = "SELECT * FROM td_pesan_hari_raya WHERE jenis_hari_raya ='imlek'";
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
    function td_get_pending() {
        $sql = "SELECT * FROM td_pesan WHERE sub_type ='pending'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_received() {
        $sql = "SELECT * FROM td_pesan WHERE sub_type ='received'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    
    function td_get_member() {
        $sql = "SELECT * FROM td_member";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    
    function cron_ultah()
    {
        $sql = "SELECT * FROM `td_member` WHERE DATE_FORMAT(tanggal_lahir, '%d%m')= DATE_FORMAT(now(), '%d%m')";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;   
    }
    
    
    //EXPORT SMS
    function td_get_member_polis_export() {
        $sql = "SELECT * FROM td_pesan_send_log WHERE CHAR_LENGTH(status) > 2 GROUP BY no_polis";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_member_sms_check() {
        $sql = "SELECT * FROM td_pesan_send_log WHERE CHAR_LENGTH(status) > 2 AND delivery_status ='1'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
   
     function td_get_member_trans() {
        $sql = "SELECT * FROM td_member_trans WHERE status='5' OR status='8' OR status='7' ORDER BY nominal_benar DESC";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
   //START SMS FUNCTION//
    function td_get_member_polis() {
        $sql = "SELECT * FROM td_member WHERE status='5' OR status='8' OR status='7' limit 1";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_member_claim_pay_blast() {
        $sql = "SELECT * FROM td_member WHERE status='5' OR status='8' OR status='7'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function td_get_member_claim_received_blast() {
        $sql = "SELECT * FROM td_member WHERE status='5' OR status='8' OR status='7'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function td_get_imlek_blast() {
        $sql = "SELECT * FROM td_member_hari_raya_imlek WHERE status='5' OR status='8' OR status='7' OR status='' LIMIT 1";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_all_member($q) {
        $sql = "SELECT * FROM `td_member` $q";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_all_member_payment($q) {
        $sql = "SELECT * FROM `td_member_trans` $q";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
     function get_all_member_received($q) {
        $sql = "SELECT * FROM `td_member` $q";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function get_all_member_imlek($q) {
        $sql = "SELECT * FROM `td_member_hari_raya_imlek` $q";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
      function get_member_by_no($n) {
        $sql = "SELECT nik FROM db_payrol WHERE CONCAT(nik,SUBSTR(REPLACE(payment_date, '/', '') ,1,11)) like'%".$n."'";

        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
//    function get_member_by_no($no_polis) {
//        $sql = "SELECT no_karyawan FROM td_member where no_karyawan like '%".$no_polis."'";
//        $query = $this->db->query($sql);
//        
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
   
     function update_payrol($no,$nik,
                        $nama,$tgl_masuk ,$tgl_keluar ,$dept ,
                        $jabatan ,
                        $cabang ,
                        $band ,
                        $bank ,
                        
                        $no_rekening ,
                        $tgl_lahir ,
                        $jns_kelamin,
                        $status_pegawai,
                        $status_pajak,
                        
                        $no_npwp,
                        $no_bpjs_kes,
                        $no_bpjs_tk,
                        $no_ktp,
                        $alamat_rumah,
                        
                        $alamat_email,
                        $bulan,
                        $tahun ,
                        $klien,
                        $gaji_pokok,
                        
                        $rapel,
                        $transport_tetap,
                        $transport_variable,
                        $makan_variable,
                        $lembur1,
            
                        $lembur2,
                        $total_lembur,
                        $shift1,
                        $shift2,
                        $total_shift,
            
                        $insentif1,
                        $insentif2,
                        $insentif3,
                        $total_intensif,
                        $bonus,
            
                        $thr,
                        $lain_lain1,
                        $lain_lain2,
                        $lain_lain3,
                        $total_lain_lain,
            
                        $gross,
                        $jamsostek_karyawan ,
                        $jamsostek_perusahaan ,
                        $pensiun_karyawan ,
                        $pensiun_perusahaan,
            
                        $bpjs_karyawan,
                        $bpjs_perusahaan,
                        $pph21,
                        $pot_pinjam,
                        $pot_lain,
                        
                        $total_pot,
                        $thp,
                        $hrd,$payment_date,$keterangan,$n){
                if($nik != "" AND $nama !=""){
                $sql ="UPDATE db_payrol SET no ='".$no."',
                        nik='".$nik."',
                        nama='".$nama."',
                        tgl_masuk='".$tgl_masuk."' ,
                        tgl_keluar ='".$tgl_keluar."',
                        
                        dept ='".$dept."',
                        jabatan ='".$jabatan."',
                        cabang ='".$cabang."',
                        band ='".$band."',
                        bank ='".$bank."',
                        
                        no_rekening ='".$no_rekening."',
                        tgl_lahir ='".$tgl_lahir."',
                        jns_kelamin='".$jns_kelamin."',
                        status_pegawai='".$status_pegawai."',
                        status_pajak='".$status_pajak."',
                        
                        no_npwp='".$no_npwp."',
                        no_bpjs_kes='".$no_bpjs_kes."',
                        no_bpjs_tk='".$no_bpjs_tk."',
                        no_ktp='".$no_bpjs_tk."',
                        alamat_rumah='".$alamat_rumah."',
                        
                        alamat_email='".$alamat_email."',
                        bulan='".$bulan."',
                        tahun='".$tahun."',
                        klien='".$klien."',
                        gaji_pokok='".$gaji_pokok."',
                        
                        rapel='".$rapel."',
                        transport_tetap='".$transport_tetap."',
                        transport_variable='".$transport_variable."',
                        makan_variable='".$makan_variable."',
                        lembur1='".$lembur1."',
                        
                        lembur2='".$lembur2."',
                        total_lembur='".$total_lembur."',
                        shift1='".$shift1."',
                        shift2='".$shift2."',
                        total_shift='".$total_shift."',
                        
                        insentif1='".$insentif1."',
                        insentif2='".$insentif2."',
                        insentif3='".$insentif3."',
                        total_insentif='".$total_intensif."',
                        bonus='".$bonus."',
                        
                        thr='".$thr."',
                        lain_lain1='".$lain_lain1."',
                        lain_lain2='".$lain_lain2."',
                        lain_lain3='".$lain_lain3."',
                        total_lain_lain='".$total_lain_lain."',
                        
                        gross='".$gross."',
                        jamsostek_karyawan ='".$jamsostek_karyawan."',
                        jamsostek_perusahaan ='".$jamsostek_perusahaan."',
                        pensiun_karyawan ='".$pensiun_karyawan."',
                        pensiun_perusahaan='".$pensiun_perusahaan."',
                        
                        bpjs_karyawan='".$bpjs_karyawan."',
                        bpjs_perusahaan='".$bpjs_perusahaan."',
                        pph21='".$pph21."',
                        pot_pinjam='".$pot_pinjam."',
                        pot_lain='".$pot_lain."',
                        
                        total_pot='".$total_pot."',
                        thp='".$thp."',
                        hrd='".$hrd."',
                        payment_date='".$payment_date."',
                        keterangan='".$keterangan."',
                        id_admin='".$this->session->userdata('id_admin')."' WHERE
                            CONCAT(nik,SUBSTR(REPLACE(payment_date, '/', '') ,3,7)) =$n
                      ";
//                var_dump($sql);
//                exit();
                
                
               
        $this->db->query($sql);
        return TRUE;
                }
    }
    
    function upload($no,
                    $nik,
                        $nama,
                        $tgl_masuk ,
                        $tgl_keluar ,
                        
                        $dept ,
                        $jabatan ,
                        $cabang ,
                        $band ,
                        $bank ,
                        
                        $no_rekening ,
                        $tgl_lahir ,
                        $jns_kelamin,
                        $status_pegawai,
                        $status_pajak,
                        
                        $no_npwp,
                        $no_bpjs_kes,
                        $no_bpjs_tk,
                        $no_ktp,
                        $alamat_rumah,
                        
                        $alamat_email,
                        $bulan,
                        $tahun ,
                        $klien,
                        $gaji_pokok,
                        
                        $rapel,
                        $transport_tetap,
                        $transport_variable,
                        $makan_variable,
                        $lembur1,
            
                        $lembur2,
                        $total_lembur,
                        $shift1,
                        $shift2,
                        $total_shift,
            
                        $insentif1,
                        $insentif2,
                        $insentif3,
                        $total_intensif,
                        $bonus,
            
                        $thr,
                        $lain_lain1,
                        $lain_lain2,
                        $lain_lain3,
                        $total_lain_lain,
            
                        $gross,
                        $jamsostek_karyawan ,
                        $jamsostek_perusahaan ,
                        $pensiun_karyawan ,
                        $pensiun_perusahaan,
            
                        $bpjs_karyawan,
                        $bpjs_perusahaan,
                        $pph21,
                        $pot_pinjam,
                        $pot_lain,
                        
                        $total_pot,
                        $thp,
                        $hrd,$payment_date,$keterangan){
                if($nik != "" AND $nama !=""){
                $sql ="INSERT INTO 
                    db_payrol 
                       (no,
                        nik,
                        nama,
                        tgl_masuk ,
                        tgl_keluar ,
                        
                        dept ,
                        jabatan ,
                        cabang ,
                        band ,
                        bank ,
                        
                        no_rekening ,
                        tgl_lahir ,
                        jns_kelamin,
                        status_pegawai,
                        status_pajak,
                        
                        no_npwp,
                        no_bpjs_kes,
                        no_bpjs_tk,
                        no_ktp,
                        alamat_rumah,
                        
                        alamat_email,
                        bulan,
                        tahun,
                        klien,
                        gaji_pokok,
                        
                        rapel,
                        transport_tetap,
                        transport_variable,
                        makan_variable,
                        lembur1,
                        
                        lembur2,
                        total_lembur,
                        shift1,
                        shift2,
                        total_shift,
                        
                        insentif1,
                        insentif2,
                        insentif3,
                        total_insentif,
                        bonus,
                        
                        thr,
                        lain_lain1,
                        lain_lain2,
                        lain_lain3,
                        total_lain_lain,
                        
                        gross,
                        jamsostek_karyawan ,
                        jamsostek_perusahaan ,
                        pensiun_karyawan ,
                        pensiun_perusahaan,
                        
                        bpjs_karyawan,
                        bpjs_perusahaan,
                        pph21,
                        pot_pinjam,
                        pot_lain,
                        
                        total_pot,
                        thp,
                        hrd,payment_date,keterangan,id_admin) VALUES (
                        '$no','$nik','$nama',
                        '$tgl_masuk' ,'$tgl_keluar' ,
                        '$dept' ,
                        '$jabatan' ,
                        '$cabang' ,
                        '$band' ,
                        '$bank' ,
                        '$no_rekening' ,
                        '$tgl_lahir' ,
                        '$jns_kelamin',
                        '$status_pegawai',
                        '$status_pajak',
                        '$no_npwp',
                        '$no_bpjs_kes',
                        '$no_bpjs_tk',
                        '$no_ktp',
                        '$alamat_rumah',
                        '$alamat_email',
                        '$bulan',
                        '$tahun' ,
                        '$klien',
                        '$gaji_pokok',
                        '$rapel',
                        '$transport_tetap',
                        '$transport_variable',
                        '$makan_variable',
                        '$lembur1',
                        '$lembur2',
                        '$total_lembur',
                        '$shift1',
                        '$shift2',
                        '$total_shift',
                        '$insentif1',
                        '$insentif2',
                        '$insentif3',
                        '$total_intensif',
                        '$bonus',
                        '$thr',
                        '$lain_lain1',
                        '$lain_lain2',
                        '$lain_lain3',
                        '$total_lain_lain',
                        '$gross',
                        '$jamsostek_karyawan' ,
                        '$jamsostek_perusahaan' ,
                        '$pensiun_karyawan' ,
                        '$pensiun_perusahaan',
                        '$bpjs_karyawan',
                        '$bpjs_perusahaan',
                        '$pph21',
                        '$pot_pinjam',
                        '$pot_lain',
                        '$total_pot',
                        '$thp',
                        '$hrd',
                        '$payment_date','$keterangan','".$this->session->userdata('id_admin')."')";
        $this->db->query($sql);
        return TRUE;
                }
    }
    
    function auto_id() {
        $sql = 'SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = "sms_blast_capital" AND TABLE_NAME = "td_member" ';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
    /*CLAIM PAYMENT START*/
    function td_get_member_claim_pay() {
        $sql = "SELECT * FROM td_member WHERE status='5' OR status='0' OR status='7'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_claim_pay() {
        $sql = "SELECT * FROM td_member";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_claim_pay_by_id($no_polis) {
        $sql = "SELECT * FROM td_member where no_polis like'%".$no_polis."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function upload_claim_pay($no_karyawan,$nama_karyawan,$no_hp,$nominal_salah,$nominal_benar,$nominal_refund)
    {
        $sql = "INSERT INTO td_member_trans(no_karyawan,nama_karyawan,no_hp,nominal_salah,nominal_benar,nominal_refund,status,sms_count) VALUES ('$no_karyawan','$nama_karyawan','$no_hp','$nominal_salah','$nominal_benar','$nominal_refund',8,0)";
        $this->db->query($sql);
        return TRUE;
    }
    function auto_id_claim_pay() {
        $sql = 'SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = "sms_blast_capital" AND TABLE_NAME = "td_member_claim_payment" ';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    /*CLAIM PAY END*/
    
      /*CLAIM PAYMENT RECIEVED*/
    function get_claim_received() {
        $sql = "SELECT * FROM td_member_claim_received";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_claim_received_by_id($no_polis) {
        $sql = "SELECT * FROM td_member_claim_received where no_polis like'%".$no_polis."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function upload_claim_received($no_polis,$userMobile)
    {
        $sql = "INSERT INTO td_member_claim_received(no_polis, no,sub_type,status,sms_count) VALUES ('".str_replace("#","",$no_polis)."','".str_replace("#","",$userMobile)."','claim_received',8,0)";
        $this->db->query($sql);
        return TRUE;
    }
    function auto_id_claim_received() {
        $sql = 'SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = "sms_blast_capital" AND TABLE_NAME = "td_member_claim_received" ';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    /*CLAIM RECIEVED END*/
    
      function get_imlek_by_id($no_polis) {
        $sql = "SELECT * FROM td_member_hari_raya_imlek where no_polis like'%".$no_polis."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function upload_imlek($no_polis,$jenis_kelamin,$nama_pemegang_polis,$agama,$produk,$no_hp)
    {
        $sql = "INSERT INTO td_member_hari_raya_imlek(no_polis, jenis_kelamin,nama_pemegang_polis,agama, produk,no_hp,sub_type,status,sms_count) VALUES ('".str_replace("#","",$no_polis)."','$jenis_kelamin','$nama_pemegang_polis','$agama','$produk','".str_replace("#","",$no_hp)."','imlek',8,0)";
        $this->db->query($sql);
        return TRUE;
    }

}