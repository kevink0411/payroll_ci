<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mpayrol extends CI_Model {

    var $table = 'db_payrol';
    var $column_order = array('id_payrol','nama',null);
    var $column_search = array('id_payrol','nama',);
    var $order = array('id_payrol' => 'desc');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $this->db->from($this->table);
        $this->db->limit(100);
        $i = 0;
        $this->db->where('id_admin',$this->session->userdata('id_admin'));
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        
        $this->_get_datatables_query();
       
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id) {
        $this->db->from($this->table);
        $this->db->where('id_payrol', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id) {
        $this->db->where('id_payrol', $id);
        $this->db->delete($this->table);
    }
    
    function td_get_member() {
        $sql = "SELECT * FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin');
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function export_all_data() {
        $sql = "SELECT * FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin');
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function bank_surat() {
        $sql = "SELECT nik,nama,bank,no_rekening,thp,klien,bulan FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin');
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function bank_surat_bulanan($client) {
        $sql = "SELECT nik,nama,bank,no_rekening,thp,klien,bulan,  payment_date,keterangan,  hrd,    
        (SELECT SUM(thp) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_THP
        FROM db_payrol WHERE id_admin='".$this->session->userdata('id_admin')."' AND bulan='".$_GET['bulan']."' $client";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function data_pajak() {
        $sql = "SELECT * FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin');
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function data_pajak_bulanan($client) {
        $sql = "SELECT *,SUM(gaji_pokok)AS SUM_GAJI,SUM(rapel)AS SUM_RAPEL,SUM(transport_tetap)AS SUM_TRANSPORT_TETAP,SUM(transport_variable)AS SUM_TRANSPORT_VARIABLE,SUM(makan_variable)AS SUM_MAKAN_VARIABLE,SUM(lembur1)AS SUM_LEMBUR1,SUM(lembur2)AS SUM_LEMBUR2,SUM(total_lembur)AS SUM_TOTAL_LEMBUR,SUM(shift1)AS SUM_SHIFT1,SUM(shift2)AS SUM_SHIFT2,SUM(total_shift)AS SUM_TOTAL_SHIFT,SUM(insentif1)AS SUM_INSENTIF1,SUM(insentif2)AS SUM_INSENTIF2,SUM(insentif3)AS SUM_INSENTIF3,SUM(total_insentif)AS SUM_TOTAL_INSENTIF,SUM(bonus)AS SUM_BONUS,SUM(thr)AS SUM_THR,
            SUM(lain_lain1)AS SUM_LAIN_LAIN1,SUM(lain_lain2)AS SUM_LAIN_LAIN2,SUM(lain_lain3)AS SUM_LAIN_LAIN3,
            SUM(total_lain_lain)AS SUM_TOTAL_LAIN_LAIN,SUM(gross)AS SUM_GROSS,SUM(jamsostek_karyawan)AS SUM_JAMSOSTEK_KARYAWAN,SUM(jamsostek_perusahaan)AS SUM_JAMSOSTEK_PERUSAHAAN,SUM(pensiun_karyawan)AS SUM_PENSIUN_KARYAWAN,SUM(pensiun_perusahaan)AS SUM_PENSIUN_PERUSAHAAN,SUM(bpjs_karyawan)AS SUM_BPJS_KARYAWAN,SUM(bpjs_perusahaan)AS SUM_BPJS_PERUSAHAAN,SUM(pph21)AS SUM_PPH21,SUM(thp)AS SUM_THP,SUM(pot_pinjam)AS SUM_POT_PINJAM,SUM(pot_lain)AS SUM_POT_LAIN,SUM(total_pot)AS SUM_TOTAL_POT,
        (SELECT SUM(gaji_pokok) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GAJI_POKOK,
        (SELECT SUM(rapel+transport_tetap+transport_variable+total_lembur+total_shift+total_lain_lain) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_RAPEL,       
        (SELECT SUM(total_insentif) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_INSENTIF,    
        (SELECT SUM(gross) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GROSS,         
        (SELECT SUM(jamsostek_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_KARYAWAN,         
        (SELECT SUM(jamsostek_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_PERUSAHAAN,         
        (SELECT SUM(pensiun_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_KARYAWAN,        
		(SELECT SUM(pensiun_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_PERUSAHAAN,        
        (SELECT SUM(bpjs_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_KARYAWAN,         
        (SELECT SUM(bpjs_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_PERUSAHAAN,        
        (SELECT SUM(pph21) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PPH21         

        FROM db_payrol WHERE id_admin='".$this->session->userdata('id_admin')."' AND bulan='".$_GET['bulan']."'$client GROUP BY nik";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function slip_report_all() {
        
        $sql = "SELECT * FROM `db_payrol` WHERE id_admin ='".$this->session->userdata('id_admin')."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();}
        return $data;
    }
    
     function get_nik($nix) {
        
        $sql = "SELECT * FROM `db_payrol` WHERE nik!='' AND nik='".$nix."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();}
        return $data;
    }
    
    function slip_report_all_perbulan($client) {
        
        $sql = "SELECT * FROM `db_payrol` WHERE id_admin ='".$this->session->userdata('id_admin')."' AND bulan='".$_GET['bulan']."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();}
        return $data;
    }
    
    function export_all_data_perbulan($client) {
        $sql = "SELECT *,SUM(gaji_pokok)AS SUM_GAJI,SUM(rapel)AS SUM_RAPEL,SUM(transport_tetap)AS SUM_TRANSPORT_TETAP,SUM(transport_variable)AS SUM_TRANSPORT_VARIABLE,SUM(makan_variable)AS SUM_MAKAN_VARIABLE,SUM(lembur1)AS SUM_LEMBUR1,SUM(lembur2)AS SUM_LEMBUR2,SUM(total_lembur)AS SUM_TOTAL_LEMBUR,SUM(shift1)AS SUM_SHIFT1,SUM(shift2)AS SUM_SHIFT2,SUM(total_shift)AS SUM_TOTAL_SHIFT,SUM(insentif1)AS SUM_INSENTIF1,SUM(insentif2)AS SUM_INSENTIF2,SUM(insentif3)AS SUM_INSENTIF3,SUM(total_insentif)AS SUM_TOTAL_INSENTIF,SUM(bonus)AS SUM_BONUS,SUM(thr)AS SUM_THR,
            SUM(lain_lain1)AS SUM_LAIN_LAIN1,SUM(lain_lain2)AS SUM_LAIN_LAIN2,SUM(lain_lain3)AS SUM_LAIN_LAIN3,
            SUM(total_lain_lain)AS SUM_TOTAL_LAIN_LAIN,SUM(gross)AS SUM_GROSS,SUM(jamsostek_karyawan)AS SUM_JAMSOSTEK_KARYAWAN,SUM(jamsostek_perusahaan)AS SUM_JAMSOSTEK_PERUSAHAAN,SUM(pensiun_karyawan)AS SUM_PENSIUN_KARYAWAN,SUM(pensiun_perusahaan)AS SUM_PENSIUN_PERUSAHAAN,SUM(bpjs_karyawan)AS SUM_BPJS_KARYAWAN,SUM(bpjs_perusahaan)AS SUM_BPJS_PERUSAHAAN,SUM(pph21)AS SUM_PPH21,SUM(thp)AS SUM_THP,SUM(pot_pinjam)AS SUM_POT_PINJAM,SUM(pot_lain)AS SUM_POT_LAIN,SUM(total_pot)AS SUM_TOTAL_POT,
               (SELECT SUM(gaji_pokok) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GAPOK,
               (SELECT SUM(rapel) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_RAPEL,
               (SELECT SUM(transport_tetap) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_TETAP,
               (SELECT SUM(transport_variable) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_VARIABLE,
               (SELECT SUM(makan_variable) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_MAKAN_VARIABLE,
               (SELECT SUM(total_lembur) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_LEMBUR,
               (SELECT SUM(total_shift) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_SHIFT,
               (SELECT SUM(total_insentif) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_INSENTIF,
               (SELECT SUM(bonus) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_BONUS,
               (SELECT SUM(thr) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_THR,
               (SELECT SUM(total_lain_lain) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_LAIN_LAIN,
               (SELECT SUM(gross) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GROSS,
               (SELECT SUM(jamsostek_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_KARYAWAN,
               (SELECT SUM(jamsostek_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_PERUSAHAAN,
               (SELECT SUM(pensiun_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_KARYAWAN,
               (SELECT SUM(pensiun_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_PERUSAHAAN,
               (SELECT SUM(bpjs_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_KARYAWAN,
               (SELECT SUM(bpjs_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_PERUSAHAAN,
               (SELECT SUM(pph21) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PPH21,
               (SELECT SUM(thp) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_THP

               FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client GROUP BY nik";
        $query = $this->db->query($sql);
       
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function summary($client) {
        
        $sql = "SELECT bank ,COUNT(*) as TOTAL,SUM(thp) AS THP, klien,payment_date,keterangan,hrd,
               (SELECT COUNT(*) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." $client) AS TOTAL_KARYAWAN,
               (SELECT SUM(thp) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." $client)AS TOTAL_THP
                FROM `db_payrol` WHERE  id_admin ='".$this->session->userdata('id_admin')."' $client GROUP BY bank,klien ORDER BY klien ASC";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function slip_report($surat) {
        if($_GET['tanggal']!="")
        {
            $tang="AND payment_date='".$_GET['tanggal']."'";
            $q = "";
        }
        else {
            $tang="";
            $q = "SUBSTR(payment_date,4,7)='".date('m/Y')."' AND";
        }
        //$sql = "SELECT hrd,bank,keterangan,payment_date, COUNT(*) as TOTAL,
          //     (SELECT SUM(thp) FROM db_payrol WHERE $q id_admin ='".$this->session->userdata('id_admin')."' AND bank='".$_GET['bank']."') AS TOTAL_ALL,SUM(thp) AS GAJI, klien FROM `db_payrol` WHERE $q hrd='".$surat."' AND id_admin ='".$this->session->userdata('id_admin')."' AND bank='".$_GET['bank']."' $tang ";
        $sql = "SELECT hrd,bank,keterangan,payment_date,thp,
			   COUNT(*)  AS TOTAL,
               (SELECT SUM(thp) FROM db_payrol WHERE $q hrd='".$surat."' AND id_admin ='".$this->session->userdata('id_admin')."' AND bank='".$_GET['bank']."' $tang) AS TOTAL_ALL,
			   SUM(thp)  AS GAJI, 
			    klien FROM `db_payrol` WHERE $q hrd='".$surat."' AND id_admin ='".$this->session->userdata('id_admin')."' AND bank='".$_GET['bank']."' $tang GROUP BY payment_date";
        $query = $this->db->query($sql);
       
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function slip_report_bulanan($client) {
        $sql = "SELECT *,SUM(gaji_pokok)AS SUM_GAJI_POKOK,SUM(rapel)AS SUM_RAPEL,SUM(transport_tetap)AS SUM_TRANSPORT_TETAP,SUM(transport_variable)AS SUM_TRANSPORT_VARIABLE,SUM(makan_variable)AS SUM_MAKAN_VARIABLE,
		SUM(lembur1)AS SUM_LEMBUR1,SUM(lembur2)AS SUM_LEMBUR2,SUM(total_lembur)AS SUM_TOTAL_LEMBUR,SUM(shift1)AS SUM_SHIFT1,
		SUM(shift2)AS SUM_SHIFT2,SUM(total_shift)AS SUM_TOTAL_SHIFT,SUM(insentif1)AS SUM_INSENTIF1,SUM(insentif2)AS SUM_INSENTIF2,SUM(insentif3)AS SUM_INSENTIF3,SUM(total_insentif)AS SUM_TOTAL_INSENTIF,SUM(bonus)AS SUM_BONUS,SUM(thr)AS SUM_THR,
            SUM(lain_lain1)AS SUM_LAIN_LAIN1,SUM(lain_lain2)AS SUM_LAIN_LAIN2,SUM(lain_lain3)AS SUM_LAIN_LAIN3,
            SUM(total_lain_lain)AS SUM_TOTAL_LAIN_LAIN,SUM(gross)AS SUM_GROSS,SUM(jamsostek_karyawan)AS 
			SUM_JAMSOSTEK_KARYAWAN,SUM(jamsostek_perusahaan)AS SUM_JAMSOSTEK_PERUSAHAAN,SUM(pensiun_karyawan)AS SUM_PENSIUN_KARYAWAN,
			SUM(pensiun_perusahaan)AS SUM_PENSIUN_PERUSAHAAN,SUM(bpjs_karyawan)AS SUM_BPJS_KARYAWAN,SUM(bpjs_perusahaan)AS SUM_BPJS_PERUSAHAAN,
			SUM(pph21)AS SUM_PPH21,SUM(thp)AS SUM_THP,SUM(pot_pinjam)AS SUM_POT_PINJAM,SUM(pot_lain)AS SUM_POT_LAIN,SUM(total_pot)AS SUM_TOTAL_POT,
        (SELECT SUM(transport_tetap) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_TETAP,
        (SELECT SUM(transport_variable) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_TRANSPORT_VARIABLE,
        (SELECT SUM(makan_variable) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_MAKAN_VARIABLE,
        (SELECT SUM(total_lembur) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_LEMBUR,
        (SELECT SUM(total_shift) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_SHIFT,
        (SELECT SUM(total_insentif) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_INSENTIF,
        (SELECT SUM(bonus) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BONUS,
        (SELECT SUM(jamsostek_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_KARYAWAN,
        (SELECT SUM(bpjs_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_KARYAWAN,
        (SELECT SUM(pensiun_karyawan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_KARYAWAN,
        (SELECT SUM(thr) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_THR,
        (SELECT SUM(total_lain_lain) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_LAIN_LAIN,
        (SELECT SUM(rapel) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_RAPEL,
        (SELECT SUM(gross) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GROSS,
        (SELECT SUM(jamsostek_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_JAMSOSTEK_PERUSAHAAN,
        (SELECT SUM(bpjs_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_BPJS_PERUSAHAAN,
        (SELECT SUM(pensiun_perusahaan) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PENSIUN_PERUSAHAN,
        (SELECT SUM(pph21) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_PPH21,
        (SELECT SUM(pot_pinjam) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_POT_PINJAM,
        (SELECT SUM(pot_lain) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_POTONGAN_LAIN,
        (SELECT SUM(total_pot) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_POTONGAN,
        (SELECT SUM(thp) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_THP,
                   
        (SELECT SUM(gaji_pokok) FROM db_payrol WHERE id_admin=".$this->session->userdata('id_admin')." AND bulan='".$_GET['bulan']."' $client)AS TOTAL_GAPOK

        FROM `db_payrol` WHERE id_admin ='".$this->session->userdata('id_admin')."' AND bulan='".$_GET['bulan']."' $client GROUP BY nik";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function bank_surat_perbank() {
          if($_GET['tanggal']!="")
    {
        $tang="AND payment_date='".$_GET['tanggal']."'";
        $q ="";
    }
    else {
        $tang="";
        $q = "SUBSTR(payment_date,4,7)='".date('m/Y')."'AND";
    }
        $sql = "SELECT hrd,nik,nama,bank,no_rekening,thp,klien,bulan,thp AS TOTAL,keterangan,
        (SELECT SUM(thp) FROM db_payrol WHERE $q id_admin=".$this->session->userdata('id_admin')." AND bank='".$_GET['bank']."' $tang)AS TOTAL_THP
        FROM db_payrol WHERE $q id_admin='".$this->session->userdata('id_admin')."' AND bank='".$_GET['bank']."' $tang";
        $query = $this->db->query($sql);
       
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function getbank() {
        $sql = "SELECT bank FROM `db_payrol` WHERE  id_admin ='".$this->session->userdata('id_admin')."' GROUP BY bank";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function getbulan() {
        $sql = "SELECT bulan FROM `db_payrol` WHERE id_admin ='".$this->session->userdata('id_admin')."' GROUP BY bulan";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function getclient() {
        $sql = "SELECT klien FROM `db_payrol` WHERE id_admin ='".$this->session->userdata('id_admin')."' GROUP BY klien";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function cms_list_subcat($parent_id) {
        $sql = "SELECT a.*,b.* FROM db_payrol AS a INNER JOIN db_payrol AS b ON 'a.klien' != '' WHERE a.bulan ='$parent_id' AND b.bulan ='$parent_id' AND a.id_admin ='".$this->session->userdata('id_admin')."'";  
         $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    
    function cms_list_bulan() {
        $sql = "SELECT bulan,klien FROM db_payrol WHERE id_admin ='".$this->session->userdata('id_admin')."' GROUP BY bulan";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    
    function cms_list_klien($bulan) {
        $sql = "SELECT bulan,klien FROM db_payrol WHERE bulan ='$bulan' AND id_admin ='".$this->session->userdata('id_admin')."' GROUP BY klien";
         $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    
    function cms_list_all_klien() {
        $sql = "SELECT bulan,klien FROM db_payrol WHERE id_admin ='".$this->session->userdata('id_admin')."' ORDER BY id_payrol ASC";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    
//    function cms_list_subcat($bulan) {
//        $sql = "SELECT bulan,klien FROM db_payrol WHERE bulan ='$bulan' GROUP BY klien";  
//        $query = $this->db->query($sql);
//        $result = $query->result_array();
//        $this->db->close();
//        return $result;
//    }
    
    /*MONITORING*/
    function getmonitordata() {
        $sql = "SELECT COUNT(a.klien) AS TOTAL_KARYAWAN,SUM(a.thp) AS TOTAL_PENGELUARAN,a.klien AS KLIEN,b.nama_lengkap AS NAMA from db_payrol as a INNER JOIN td_admin AS b ON a.id_admin = b.id_admin GROUP BY a.klien,a.id_admin";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function foo($bar) 
    { 
        $x = 5; 
        $y = foo($x);
        $qux = $bar; 
        $bar += 1; 
        return $qux; 
        
    }

    function delete_all() {
        $sql = "TRUNCATE TABLE `db_payrol`";
        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }

    function delete_all_by_id() {
        
        $sql = "DELETE FROM `db_payrol` WHERE id_admin =".$this->session->userdata('id_admin');
        $this->db->query($sql);
        $this->db->close();
        return TRUE;
    }
        
}