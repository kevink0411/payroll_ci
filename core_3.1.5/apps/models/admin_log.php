<?php

class Admin_log extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function log_login() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Login','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_logout() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Logout','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_spaj() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add SPAJ SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_spaj() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit SPAJ SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_polis() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add POLIS SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_polis() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit POLIS SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_billing_payment() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add Billing Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_billing_payment() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit Billing Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_claim_payment() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add Claim Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_claim_payment() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit Claim Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_claim_pending() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add Claim Pending SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_claim_received() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add Claim Received SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_claim_pending() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit Claim Pending SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_claim_received() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit Claim Received SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_ultah() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add ULTAH SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_ultah() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit ULTAH SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_mature() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add MATURE SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_mature() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit MATURE SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_ahli_waris() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add AHLI WARIS SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_ahli_waris() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit AHLI WARIS SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_alamat() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add ALAMAT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_alamat() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit ALAMAT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_aro() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add ARO SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_aro() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit ARO SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_marketing() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add MARKETING SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_marketing() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit MARKETING SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_no_rek() {
//        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
//        $json = json_decode($location, true);
//        if($json['region_name'] != "")
//        {$region =$json['region_name'];}
//        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add NO REK SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_no_rek() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit NO REK SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_surender() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add SURENDER SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_surender() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit SURENDER SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_hari_raya() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add HARI RAYA SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_hari_raya() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit HARI RAYA SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_delete_hari_raya() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete HARI RAYA SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_surender_unit() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add SURENDER UNIT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_surender_unit() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit SURENDER UNIT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_freelook() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add FREELOOK SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_freelook() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit FREELOOK SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_lapsed() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add LAPSED SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_lapsed() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit LAPSED SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_reinstat() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add REINSTAT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_reinstat() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit REINSTAT SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_switching() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add SWITCHING SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_switching() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit SWITCHING SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_topup() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add TOPUP SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_topup() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit TOPUP SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_add_withdraw() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Add WITHDRAW SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

    function log_edit_withdraw() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Edit WITHDRAW SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
    
    function log_delete_billing_payment() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Billing Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_claim_payment() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Claim Payment SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_claim_pending() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Claim Pending SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_claim_received() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Claim Received SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
     function log_delete_polis() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Polis SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
     function log_delete_spaj() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete SPAJ SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
    function log_delete_ahliwaris() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Ahli Waris SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_alamat() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Alamat SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
    function log_delete_aro() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete ARO SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_marketing() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Marketing SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
    function log_delete_mature() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete mature SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_norek() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete No REK SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_surender() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Surender SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_ultah() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Ultah SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    function log_delete_unit_freelook() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Freelook SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
     function log_delete_unit_lapsed() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Lapsed SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

     function log_delete_unit_reinstat() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Reinstat SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_unit_surender() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Unit Surender SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_unit_switching() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Switching SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function log_delete_unit_topup() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Top UP SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }

     function log_delete_unit_withdraw() {
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();
        $sql = "INSERT INTO td_admin_log(keterangan,nama_lengkap,ip_address,browser,platform) VALUES ('Delete Withdraw SMS Template','" . $this->session->userdata['nama_lengkap'] . "','" . $this->input->ip_address() . "','$agent','$platform.')";
        $this->db->query($sql);
        return TRUE;
    }



}
