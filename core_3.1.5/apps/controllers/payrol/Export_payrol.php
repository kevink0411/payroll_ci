<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_payrol extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','weburi','header','file','contentdate','stringurl','textlimiter','security'));
      
        $this->load->model(array('mpayrol','admin_log'));
        $this->load->library('form_validation');
        $this->is_logged_in();
    }
    
    function export_alldata()
    {
        $t['alldata'] = $this->mpayrol->export_all_data();
        $this->load->view('report/export',$t, FALSE);
    }
    function export_bank_surat()
    {
        $t['alldata'] = $this->mpayrol->bank_surat();
        $this->load->view('report/export_bank',$t, FALSE);
    }
    
    function export_data_bank()
    {
//        var_dump($_GET['form']);
//        exit();
       //$pesan ="";
        if($_GET['form']=='surat1'){
            $surat ='VSDM';
           $t['perbank'] = $this->mpayrol->slip_report($surat);
          
            if(count($t['perbank'])!=0 ) {          
                 $this->load->view('report/export_perbank',$t, FALSE);
            }
            else {
               echo "<script>
                alert('There are no fields to generate a report');
                window.location.href='".base_url()."payrol-data';
                </script>";
            }
        }
        else if($_GET['form']=='surat2'){
            $surat ='VSI';
           $t['perbank'] = $this->mpayrol->slip_report($surat);
          
            if(count($t['perbank'])!=0 ) {          
                 $this->load->view('report/export_perbank',$t, FALSE);
            }
            else {
               echo "<script>
                alert('There are no fields to generate a report');
                window.location.href='".base_url()."payrol-data';
                </script>";
            }
        }
        else if($_GET['form']=='surat3'){
               $surat ='VI';
           $t['perbank'] = $this->mpayrol->slip_report($surat);
          
            if(count($t['perbank'])!=0 ) {          
                 $this->load->view('report/export_perbank',$t, FALSE);
            }
            else {
               echo "<script>
                alert('There are no fields to generate a report');
                window.location.href='".base_url()."payrol-data';
                </script>";
            }
        }
        else 
        {
            $t['alldata'] = $this->mpayrol->bank_surat_perbank();
            if(count($t['alldata'])!=0 ) {          
                 $this->load->view('report/export_bank_excel',$t, FALSE);
            }
            else {
               echo "<script>
                alert('There are no fields to generate a report');
                window.location.href='".base_url()."payrol-data';
                </script>";
            }
            
        }
    }
    
    
    
    function export_data_perbulan()
    {
       
        $klien =$_GET['klien'];
        $client = $klien=="" ? "" : "AND klien = '$klien'";  
        
        if($_GET['form']=='payrol'){
            
            $t['alldata'] = $this->mpayrol->export_all_data_perbulan($client);
            $this->load->view('report/export',$t, FALSE);
        }
        else if($_GET['form']=='slip') 
        {
            $t['alldata'] = $this->mpayrol->slip_report_bulanan($client);
            $this->load->view('report/export_slip',$t, FALSE);
        }
        else if($_GET['form']=='databankpay')
        {
            $t['alldata'] = $this->mpayrol->bank_surat_bulanan($client);
            $this->load->view('report/export_bank',$t, FALSE);
            
        }
        else if($_GET['form']=='pajak')
        {
             $t['alldata'] = $this->mpayrol->data_pajak_bulanan($client);
             $this->load->view('report/export_pajak',$t, FALSE);
        }
        else
        {
          $t['alldata'] = $this->mpayrol->summary($client);
          $this->load->view('report/export_summary',$t, FALSE);   
        }
    }
  
    
    function export_pajak()
    {
        $t['alldata'] = $this->mpayrol->data_pajak();
        $this->load->view('report/export_pajak',$t, FALSE);
    }
    
    function export_slip()
    {
        $t['alldata'] = $this->mpayrol->slip_report_all();
        $this->load->view('report/export_slip',$t, FALSE);
    }
    
    
    function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('');
        }
    }
}