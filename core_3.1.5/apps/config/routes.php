<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'login';


$route['payrol-data'] = 'payrol/list_payrol';
$route['monitoring'] = 'monitoring/monitoring';
$route['tes'] = 'tes';

$route['upload-polis'] = 'upload';
$route['upload-claim-payment'] = 'upload/upload_transvis_template';
$route['upload-claim-received'] = 'upload/upload_claim_received';
$route['upload-imlek'] = 'upload/upload_hari_raya_imlek';

$route['spaj'] = 'nbuw/spaj';
$route['polis'] = 'nbuw/polis';
$route['poliss'] = 'nbuw/polis/polis';
$route['sms'] = 'nbuw/polis/sms';
$route['check'] = 'nbuw/polis/check';
$route['ultah'] = 'ucapan/ultah';
$route['ahli-waris'] = 'pos/ahli_waris';
$route['alamat'] = 'pos/alamat';
$route['aro'] = 'pos/aro';
$route['marketing'] = 'pos/marketing';
$route['mature'] = 'pos/mature';
$route['no-rek'] = 'pos/no_rek';
$route['surender'] = 'pos/surender';


$route['surender-unit'] = 'unit_link/surender';
$route['freelook'] = 'unit_link/freelook';
$route['top-up'] = 'unit_link/topup';
$route['lapsed'] = 'unit_link/lapsed';
$route['reinstat'] = 'unit_link/reinstat_ment';
$route['switching'] = 'unit_link/switching';
$route['withdraw'] = 'unit_link/withdraw';

$route['pay-claim'] = 'claim/payment';
$route['pay-claim-blast'] = 'claim/payment/sms';
$route['pending'] = 'claim/pending';
$route['received'] = 'claim/received';
$route['received-blast'] = 'claim/received/sms';
$route['pay-bill'] = 'billing/payment';

$route['logout'] = 'login/logout';
$route['1'] = 'ucapan/hari_raya';
$route['kandidat-edit/(.*)'] = "kandidat/view_all_kandidat/edit_data_Pribadi/index/$1";

$route['export'] = 'nbuw/polis/export_to_excel';

$route['home'] = 'home';
$route['member'] = 'report/member';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
